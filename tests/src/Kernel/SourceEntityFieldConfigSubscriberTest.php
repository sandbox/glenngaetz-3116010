<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\profile\Entity\Profile;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;

/**
 * Test the SourceEntityFieldConfig entity.
 *
 * @group commerce_stock_units
 *
 */
class SourceEntityFieldConfigSubscriberTest extends CommerceStockKernelTestBase {
  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The Stock Unit Manager.
   */
  protected $stock_unit_manager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
    'test_stock_receipt',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // install the Test Stock Recept module
    $this->installEntitySchema('stock_receipt');
    $this->installEntitySchema('stock_receipt_item');
    $this->installConfig(['test_stock_receipt']);
    
    // Change the workflow of the default order type.
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);

    $this->stock_unit_manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
  }

  /**
   * Test that the event subscriber reacts on save of the config.
   *
   * The configured fields should be copied from the source entity 
   * to the state record entity.
   */
  public function testListenerReactsOnSave() {
    $data = [
      [
        'source_entity_type' => 'stock_receipt_item',
        'source_bundle' => 'stock_receipt_item',
        'source_field' => 'wholesale_cost',
        'state_field' => 'wholesale_cost',
      ],
      [
        'source_entity_type' => 'commerce_order_item',
        'source_bundle' => 'default',
        'source_field' => 'unit_price',
        'state_field' => 'unit_price',
      ]
    ];

    foreach ($data as $d) {
      $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->create([
        'id' => $d['source_entity_type'] . '.' . $d['source_bundle'] . '.' . $d['source_field'],
        'label' => 'Entity: ' . $d['source_entity_type'] . '; Bundle: ' . $d['source_bundle'] . '; Field: ' . $d['source_field'],
        'source_entity_type' => $d['source_entity_type'],
        'source_bundle' => $d['source_bundle'],
        'source_field' => $d['source_field'],
        'state_field' => $d['source_field'],
      ]);

      $config->save();
    }

    // config should have created the new fields on the state record
    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertTrue($state_record->hasField('unit_price'));

  }

  /**
   * Test that the event subscriber reacts on update of the config.
   *
   * Probably shouldn't actually be a possibility. Either a field is 
   * used or not, so it should just be create or delete.
   *
   * What possible changes are there?
   *
   */
  /*
  public function testListenerReactsOnUpdate() {
    $data = [
      [
        'source_entity_type' => 'stock_receipt_item',
        'source_bundle' => 'stock_receipt_item',
        'source_field' => 'wholesale_cost',
        'state_field' => 'wholesale_cost',
      ],
      [
        'source_entity_type' => 'commerce_order_item',
        'source_bundle' => 'default',
        'source_field' => 'unit_price',
        'state_field' => 'unit_price',
      ]
    ];

    foreach ($data as $d) {
      $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->create([
        'id' => $d['source_entity_type'] . '.' . $d['source_bundle'] . '.' . $d['source_field'],
        'label' => 'Entity: ' . $d['source_entity_type'] . '; Bundle: ' . $d['source_bundle'] . '; Field: ' . $d['source_field'],
        'source_entity_type' => $d['source_entity_type'],
        'source_bundle' => $d['source_bundle'],
        'source_field' => $d['source_field'],
        'state_field' => $d['source_field'],
      ]);

      $config->save();
    }

    // config should have created the new fields on the state record
    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertTrue($state_record->hasField('unit_price'));

  }
   */

  /**
   * Test that the event subscriber reacts on delete of the config.
   *
   * The config should be deleted but the fields and data remains intact
   * on the state record. Essentially, there is no reaction.
   */
  public function testListenerReactsOnDelete() {
    $data = [
      [
        'source_entity_type' => 'stock_receipt_item',
        'source_bundle' => 'stock_receipt_item',
        'source_field' => 'wholesale_cost',
        'state_field' => 'wholesale_cost',
      ],
      [
        'source_entity_type' => 'commerce_order_item',
        'source_bundle' => 'default',
        'source_field' => 'unit_price',
        'state_field' => 'unit_price',
      ]
    ];

    foreach ($data as $d) {
      $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->create([
        'id' => $d['source_entity_type'] . '.' . $d['source_bundle'] . '.' . $d['source_field'],
        'label' => 'Entity: ' . $d['source_entity_type'] . '; Bundle: ' . $d['source_bundle'] . '; Field: ' . $d['source_field'],
        'source_entity_type' => $d['source_entity_type'],
        'source_bundle' => $d['source_bundle'],
        'source_field' => $d['source_field'],
        'state_field' => $d['source_field'],
      ]);

      $config->save();
    }

    // config should have created the new fields on the state record
    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertTrue($state_record->hasField('unit_price'));

    // now delete the config for the wholesale cost field.
    $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->load('stock_receipt_item.stock_receipt_item.wholesale_cost');
    $config->delete();
    
    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertTrue($state_record->hasField('unit_price'));
  }

}

