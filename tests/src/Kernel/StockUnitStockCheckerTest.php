<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;
use Drupal\commerce_stock_units\StockUnitStockUpdater;

/**
 * Ensure the stock transactions are performed on order events using the 
 * commerce_stock_units service instead of the commerce_local_stock service
 *
 * @group commerce_stock_units
 */
class StockUnitsStockCheckerTest extends CommerceStockKernelTestBase {

  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The Stock Unit Manager.
   */
  protected $manager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // Change the workflow of the default order type.
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);

    // Set initial Stock level.
    $stockServiceManager->createTransaction($this->variation, $this->locations[1]->getId(), '', 10, 10.10, 'USD', StockTransactionsInterface::NEW_STOCK, []);

    $profile = Profile::create([
      'type' => 'customer',
      'uid' => $user->id(),
    ]);
    $profile->save();

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::create([
      'type' => 'default',
      'state' => 'draft',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
    ]);
    $order->save();

    $this->order = $this->reloadEntity($order);

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')
      ->getStorage('commerce_order_item');

    // Add order item.
    $order_item1 = $order_item_storage->createFromPurchasableEntity($this->variation);
    $order_item1->save();
    $order_item1 = $this->reloadEntity($order_item1);
    $order->addItem($order_item1);
    $order->save();
    $this->order = $this->reloadEntity($order);
    
    $this->manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
  }

  /**
   * Test that the stock unit stock checker is loaded instead of Local Stock Checker.
   *
   * @group basic
   * @group service
   */
  public function testServiceLoaded() {
    $this->assertEquals('StockUnitStockChecker', (new \ReflectionClass($this->checker))->getShortName());
  }

  /**
   * Test getTotalStockLevel() method.
   *
   * @group getTotalStockLevel
   */
  public function testGetTotalStockLevel() {
    $updater = StockUnitStockUpdater::create(\Drupal::getContainer());
    $stock_level = $this->checker->getTotalStockLevel($this->variation, $this->locations);
    $this->assertEquals(10, $stock_level);

    // sell one
    $transition = $this->order->getState()->getTransitions();
    $this->order->setOrderNumber('2017/01');
    $this->order->getState()->applyTransition($transition['place']);
    $this->order->save();

    $this->assertEquals(9, $this->checker->getTotalStockLevel($this->variation, $this->locations));

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type);

    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();

    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 2,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();

    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    $transition = $order->getState()->getTransitions();
    $order->getState()->applyTransition($transition['place']);
    $order->save();

    $this->assertEquals(7, $this->checker->getTotalStockLevel($this->variation, $this->locations));
  }

  /**
   * Test getTotalStockLevel() with a date.
   *
   * @group getTotalStockLevel
   * @group dated
   */
  public function testDatedGetTotalStockLevel() {
     $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type);

    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();

    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 2,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();

    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    $transition = $order->getState()->getTransitions();
    $order->getState()->applyTransition($transition['place']);
    $order->save();

    $date = $order->getCreatedTime();

    $past_date = $date - 1;
    $this->assertEquals(10, $this->checker->getTotalStockLevel($this->variation, $this->locations, $past_date));
  }

}

