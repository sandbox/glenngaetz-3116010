<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
#use Drupal\commerce_stock_units\FieldConfigHelper;

/**
 * Test the FieldConfigHelper class.
 *
 */
class FieldConfigHelperTest extends CommerceStockKernelTestBase {

  /**
   * The field config helper.
   */
  protected $field_config_helper;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
    'test_stock_receipt',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // install the Test Stock Recept module
    $this->installEntitySchema('stock_receipt');
    $this->installEntitySchema('stock_receipt_item');
    $this->installConfig(['test_stock_receipt']);

    $this->field_config_helper = \Drupal::service('commerce_stock_units.field_config_helper');

  }

  /**
   * Test getEntityFields method.
   *
   * This method just returns the entity fields that reference products.
   */
  public function testGetEntityFields() {

    $expected = [];

    foreach ([
      ['commerce_product', 'default', 'variations'], 
      ['commerce_order_item', 'default', 'purchased_entity'], 
      ['commerce_stock_units_stock_unit', 'commerce_stock_units_stock_unit', 'product_variation_id'], 
      ['stock_receipt_item', 'stock_receipt_item', 'received_entity']
    ] as $values) {
      $expected[$values[0]][$values[1]] = \Drupal::service('entity_field.manager')->getFieldDefinitions($values[0], $values[1]);
    }

    $this->assertEquals($expected, $this->field_config_helper->getEntityFields());
  }

  /**
   * Test getParentEntityFields method.
   */
  public function testGetParentEntityFields() {

    $entity_fields = [];

    foreach ([
      ['commerce_product', 'default', 'variations'], 
      ['commerce_order_item', 'default', 'purchased_entity'], 
      ['commerce_stock_units_stock_unit', 'commerce_stock_units_stock_unit', 'product_variation_id'], 
      ['stock_receipt_item', 'stock_receipt_item', 'received_entity']
    ] as $values) {   
      $entity_fields[$values[0]][$values[1]][$values[2]] = \Drupal::service('entity_field.manager')->getFieldDefinitions($values[0], $values[1])[$values[2]];
    }

    foreach ($entity_fields as $entity => $entity_data) {
      foreach ($entity_data as $bundle => $bundle_data) {
        $expected = [];
        
        $fields = \Drupal::service('entity_field.manager')->getFieldMapByFieldType('entity_reference');

        foreach ($fields as $field_entity => $field_items) {
          foreach ($field_items as $f => $v) {
            foreach ($v['bundles'] as $field_bundle) {
              $defs = \Drupal::service('entity_field.manager')->getFieldDefinitions($field_entity, $field_bundle);
              foreach ($defs as $name => $def) {
                if ($def->getType() == 'entity_reference' && $def->getSetting('target_type') == $entity) {
                  $expected[$field_entity][$field_bundle] = $defs;
                }
              }
            }
          }
        }

        $result = $this->field_config_helper->getParentEntityFields($entity, $bundle);

        print 'Expected: ' . "\n";
        print_r($expected);
        print "\n\n";
        print 'Result: ' . "\n";
        print_r($result);

        $this->assertEquals($expected, $result);
      }
    }
  }

}

