<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\commerce_stock_local\LocalStockUpdater;
use Drupal\profile\Entity\Profile;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecord;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;
use Drupal\Core\Database\IntegrityConstraintViolationException;

/**
 * Test the StockUnitStateRecord->save() method.
 *
 * @group commerce_stock_units
 */
class StockUnitStateRecordTest extends CommerceStockKernelTestBase {

  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  #protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  #protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The stock service manager.
   *
   * @var $stockServiceManager
   */
  #protected $stockServiceManager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
    'test_stock_receipt',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // install the Test Stock Recept module
    $this->installEntitySchema('stock_receipt');
    $this->installEntitySchema('stock_receipt_item');
    $this->installConfig(['test_stock_receipt']);

    // Change the workflow of the default order type.
    /*
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();
    */

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $this->stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $this->stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $this->stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);

  }

  /**
   * Test basic save of state record.
   * 
   * @group basic
   */
  public function testStockUnitStateRecordSave() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id()
    ]);

    $stock_unit->save();

    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $this->assertEquals(NULL, $state_record->id());
    $this->assertGreaterThan(0, $state_record->getEffectiveTime());

    $state_record->save();

    $this->assertEquals(1, $state_record->id());
  }

  /**
   * Test that the test module's field work properly
   * as it is used to test additional fields on the state record.
   *
   * @group fields
   */
  public function testTestStockReceiptField() {
    $wholesale_cost = new Price('10.00', 'CAD');
    $item = \Drupal::entityTypeManager()->getStorage('stock_receipt_item')->create([
      'received_entity' => $this->variation,
      'title' => 'Test receipt item',
      'quantity' => 2,
      'wholesale_cost' => $wholesale_cost,
    ]);
    $item->save();

    $this->assertTrue($item->hasField('wholesale_cost'));
    $item->save();

    $value = new Price($item->wholesale_cost->number, $item->wholesale_cost->currency_code);

    $this->assertEquals($wholesale_cost, $item->get('wholesale_cost')[0]->toPrice());
  }

  /**
   * Test basic save of state record with added field.
   * 
   * @group basic
   * @group fields
   */
  public function testStockUnitStateRecordSaveWithField() {
    
    // get the wholesale_cost field from the stock_receipt_item entity
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $fields = $entity_field_manager->getFieldStorageDefinitions('stock_receipt_item');

    $this->assertNotEmpty($fields);
    $this->assertNotEmpty($fields['wholesale_cost']);

    $new_field_storage = $fields['wholesale_cost']->createDuplicate();

    $new_field_storage->set('field_name', 'wholesale_cost');
    $new_field_storage->set('entity_type', 'commerce_stock_units_state_rcrd');

    $new_field_storage->save();

    $fields = $entity_field_manager->getFieldDefinitions('stock_receipt_item', 'stock_receipt_item');

    $new_field = $fields['wholesale_cost']->createDuplicate();
    $new_field->set('field_name', 'wholesale_cost');
    $new_field->set('entity_type', 'commerce_stock_units_state_rcrd');
    $new_field->set('bundle', 'commerce_stock_units_state_rcrd');

    $new_field->save();

    /*
    $fields = $entity_field_manager->getFieldDefinitions('commerce_stock_units_state_rcrd', 'default');
    foreach ($fields as $k => $v) {
      print 'field: ' . $k . "\n";
      print '  data type: ' . $v->getDataType() . "\n";
    }
    var_dump($fields);
     */
    
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id()
    ]);

    $stock_unit->save();

    $wholesale_cost = new Price('10.00', 'CAD');

    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_uid' => 1,
      'wholesale_cost' => $wholesale_cost,
    ]);

    $state_record->save();

    $this->assertEquals(1, $state_record->id());
    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertEquals($wholesale_cost, $state_record->get('wholesale_cost')[0]->toPrice());
  }

  /**
   * Test getLocation().
   *
   * @group basic
   */
  public function testGetLocation() {
    $state_record = $this->createStateRecord();

    $this->assertEquals(1, $state_record->getLocation()->id());
  }

  /**
   * Test getLocationZone().
   *
   * @group basic
   */
  public function testGetLocationZone() {
    $state_record = $this->createStateRecord();

    $this->assertEquals(NULL, $state_record->getLocationZone());
  }

  /**
   * Test getUnitCost().
   *
   * @group basic
   */
  public function testGetUnitCost() {
    $state_record = $this->createStateRecord();

    $this->assertEquals('10.00', $state_record->getUnitCost());
  }

  /**
   * Test getCurrencyCode().
   *
   * @group basic
   */
  public function testGetCurrencyCode() {
    $state_record = $this->createStateRecord();

    $this->assertEquals('CAD', $state_record->getCurrencyCode());
  }

  /**
   * Test getTransactionTime().
   *
   * @group basic
   */
  public function testGetEffectiveTime() {
    $effective_time = time();
    $state_record = $this->createStateRecord($effective_time);

    $this->assertEquals($effective_time, $state_record->getEffectiveTime());
  }

  /**
   * Test getState().
   *
   * @group basic
   */
  public function testGetState() {
    $state_record = $this->createStateRecord();

    $this->assertEquals(StockUnitStateRecord::AVAILABLE, $state_record->getState());
  }

  /**
   * Test getSourceDocument().
   *
   * @group basic
   */
  public function testGetSourceDocument() {
    $state_record = $this->createStateRecord();

    $this->assertEquals(1, $state_record->getSourceDocument()->id());
  }

  /**
   * Test id().
   *
   * @group basic
   */
  public function testID() {
    $state_record = $this->createStateRecord();

    $this->assertEquals(1, $state_record->id());
  }


  /**
   * Test the getNextStates() method.
   */
  public function testGetNextStates() {
    $state_record = $this->createStateRecord();

    // Available with no future states
    $current = StockUnitStateRecordInterface::AVAILABLE;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::AVAILABLE,
      StockUnitStateRecordInterface::HOLD,
      StockUnitStateRecordInterface::SOLD,
      StockUnitStateRecordInterface::DEFECTIVE,
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::LOST,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertequals($expected, $next);

    // Available with future sold
    $current = StockUnitStateRecordInterface::AVAILABLE;
    $future = [
      StockUnitStateRecordInterface::SOLD,
    ];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertequals($expected, $next);

    // Hold
    $current = StockUnitStateRecordInterface::HOLD;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::HOLD,
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $next);
    
    // Hold with future state
    $current = StockUnitStateRecordInterface::HOLD;
    $future = [
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::HOLD,
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $next);
    
    // Sold
    $current = StockUnitStateRecordInterface::SOLD;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::SOLD,
      StockUnitStateRecordInterface::AVAILABLE,
      StockUnitStateRecordInterface::SHIPPED,
    ];
    $this->assertEquals($expected, $next);
    
    // Shipped
    $current = StockUnitStateRecordInterface::SHIPPED;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::SHIPPED,
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $next);

    // Defective
    $current = StockUnitStateRecordInterface::DEFECTIVE;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::DEFECTIVE,
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $next);

    // Returned
    $current = StockUnitStateRecordInterface::RETURNED;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $next);

    // Destroyed
    $current = StockUnitStateRecordInterface::DESTROYED;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $next);

    // Lost
    $current = StockUnitStateRecordInterface::LOST;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::LOST,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $next);

    // Deleted
    $current = StockUnitStateRecordInterface::DELETED;
    $future = [];
    $next = $state_record->getNextStates($current, $future);
    $expected = [
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $next);

  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown() {
    $connection = $this->container->get('database');

    $connection->delete('commerce_stock_units_state_record')->execute();
    $connection->delete('commerce_stock_units_stock_unit')->execute();
    $connection->query('ALTER TABLE {commerce_stock_units_state_record} AUTO_INCREMENT = 1')->execute();
    $connection->query('ALTER TABLE {commerce_stock_units_stock_unit} AUTO_INCREMENT = 1')->execute();
    parent::tearDown();
  }

  /**
   * Set up a state record for testing.
   */
  private function createStateRecord(int $effective_time = NULL, int $state = NULL) {
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;

    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    
    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    $effective_time = $effective_time ? $effective_time : time();
    $state = $state ? $state : StockUnitStateRecord::AVAILABLE;
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id()
    ]);

    $stock_unit->save();

    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $effective_time,
      'state' => $state,
      'related_transaction' => 25,
      'related_transacton' => $transaction_id,
      'source_document_id' => $order->id(),
      'source_document_type' => $order->getEntityTypeId(),
      'related_user' => 1,
    ]);

    $state_record->save();

    return $state_record;
  }

}

