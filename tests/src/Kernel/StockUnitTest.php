<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnit;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecord;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;

/**
 * Ensure the stock transactions are performed on order events using the 
 * commerce_stock_units service instead of the commerce_local_stock service
 *
 * @group commerce_stock_units
 */
class StockUnitTest extends CommerceStockKernelTestBase {

  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The stock service manager.
   *
   * @var $stockServiceManager
   */
  protected $stockServiceManager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // Change the workflow of the default order type.
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $this->stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $this->stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $this->stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);
  }

  /**
   * Test that a stock unit can be created.
   *
   * @group basic
   */
  public function testCreateNewStockUnit() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);
    
    $this->assertEquals(NULL, $stock_unit->id());
  }

  /**
   * Test that saving the stock unit works
   * 
   * @group basic
   */
  public function testCreateAndSaveNewStockUnit() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $this->assertEquals(1, $stock_unit->id());
  }

  /**
   * Test that a stock unit can be created and a state added.
   *
   * @group state
   */
  public function testCreateNewStockUnitWithState() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $state_timestamp = time();
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location' => $this->locations[1]->id(),
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $state_timestamp,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $state_records = $stock_unit->get('state')->referencedEntities();
    $this->assertEquals(1, $state_records[0]->id());
    $this->assertEquals(NULL, $state_records[0]->getLocationZone());
    $this->assertEquals('10.00', $state_records[0]->getUnitCost());
    $this->assertEquals('CAD', $state_records[0]->getCurrencyCode());
    $this->assertEquals($state_timestamp, $state_records[0]->getTransactionTime());
    $this->assertEquals(StockUnitStateRecord::AVAILABLE, $state_records[0]->getState());
    $this->assertEquals(1, $state_records[0]->getLocation()->id());
  }

  /**
   * Test adding an additional new state to a stock unit.
   *
   * @group state
   */
  public function testAddNewState() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'timestamp' => $time1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $time2 = time();
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'timestamp' => $time2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save(); 
   
    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $state_records = $stock_unit->get('state')->referencedEntities();
    $this->assertEquals(2, count($state_records));
  }

  /**
   * Test retrieving the latest state.
   *
   * @group state
   * @group getState
   */
  public function testGetStateNoDate() {
    // this should get the most recent state
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time1,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $time2 = time();
    $state_record2 = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time2,
      'transaction_id' => 2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record2->save(); 
   
    $stock_unit->state[] = $state_record2;
    $stock_unit->save();

    $state = $stock_unit->getState();

    $this->assertEquals($state_record2->id(), $state->id());
    $this->assertEquals($time2, $state->getTransactionTime());
  }

  /**
   * Test retrieving a past state.
   *
   * @group state
   * @group getState
   */
  public function testGetStatePastDate() {
    // this should get the most recent state
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time1,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    sleep(10);

    $past_time = time();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    sleep(6);
    $time2 = time();
    $state_record2 = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time2,
      'transaction_id' => 2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record2->save(); 
   
    $stock_unit->state[] = $state_record2;
    $stock_unit->save();

    $state = $stock_unit->getState($past_time);

    $this->assertEquals($state_record->id(), $state->id());
    $this->assertEquals($time1, $state->getTransactionTime());
  }

  /**
   * Test retrieving the original (probably first) state.
   *
   * @group state
   * @group getOriginalState
   */
  public function testGetOriginalState() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time1,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    sleep(10);

    $past_time = time();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    sleep(6);
    $time2 = time();
    $state_record2 = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time2,
      'transaction_id' => 2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record2->save(); 
   
    $stock_unit->state[] = $state_record2;
    $stock_unit->save();

    $state = $stock_unit->getOriginalState();

    $this->assertEquals($state_record->id(), $state->id());
    $this->assertEquals($time1, $state->getTransactionTime());
  }


  /**
   * Test retrieving the state history.
   *
   * @group state
   * @group getStateHistory
   */
  public function testGetStateHistory() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time1,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    sleep(10);

    $past_time = time();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    sleep(6);
    $time2 = time();
    $state_record2 = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time2,
      'transaction_id' => 2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record2->save(); 
   
    $stock_unit->state[] = $state_record2;
    $stock_unit->save();

    $state = $stock_unit->getStateHistory();

    $this->assertEquals(2, count($state));
    $this->assertEquals($state_record->id(), $state[$time1]->id());
    $this->assertEquals($time1, $state[$time1]->getTransactionTime());
    $this->assertEquals($state_record2->id(), $state[$time2]->id());
    $this->assertEquals($time2, $state[$time2]->getTransactionTime());
  }

  /**
   * Test retrieving the possible new states for a new state record 
   *
   * @group state
   * @group getPossibleStates
   */
  public function testGetPossibleStates() {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $this->variation->id(),
    ]);

    $stock_unit->save();

    // Available with no future states
    $time1 = time(); 
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $time1,
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::AVAILABLE,
      StockUnitStateRecordInterface::HOLD,
      StockUnitStateRecordInterface::SOLD,
      StockUnitStateRecordInterface::DEFECTIVE,
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::LOST,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertequals($expected, $possible);

    // Available with future sold
    $future = strtotime('+1 month');
    $state_record2 = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => $future,
      'transaction_id' => 2,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record2->save(); 
   
    $stock_unit->state[] = $state_record2;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertequals($expected, $possible);

    // reset the stock unit's state
    $stock_unit->state = [];
    
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    // Hold
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::HOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::HOLD,
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $possible);
    
    // Hold with future state
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => strtotime('+1 month'),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::HOLD,
      //StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $possible);
    
    // reset the stock unit's state
    $stock_unit->state = [];
    
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    // Sold
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::SOLD,
      StockUnitStateRecordInterface::AVAILABLE,
      StockUnitStateRecordInterface::SHIPPED,
    ];
    $this->assertEquals($expected, $possible);
    
    // Sold with future state
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => strtotime('+1 month'),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::SHIPPED,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::SOLD,
    ];
    $this->assertEquals($expected, $possible);

    // reset the stock unit's state
    $stock_unit->state = [];
    
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    // Shipped
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::SOLD,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();
    
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::SHIPPED,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::SHIPPED,
      StockUnitStateRecordInterface::AVAILABLE,
    ];
    $this->assertEquals($expected, $possible);

    // reset the stock unit's state
    $stock_unit->state = [];
    
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::AVAILABLE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    // Defective
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::DEFECTIVE,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::DEFECTIVE,
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $possible);

    // Returned
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::RETURNED,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
 
    $expected = [
      StockUnitStateRecordInterface::RETURNED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $possible);

    // Destroyed
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::DESTROYED,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();

    $expected = [
      StockUnitStateRecordInterface::DESTROYED,
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $possible);

    // Lost
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::LOST,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::LOST,
      StockUnitStateRecordInterface::DELETED,    
    ];
    $this->assertEquals($expected, $possible);

    // Deleted
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'stock_unit_id' => $stock_unit->id(),
      'location_id' => 1,
      'location_zone' => NULL,
      'unit_cost' => '10.00',
      'currency_code' => 'CAD',
      'effective_time' => time(),
      'transaction_id' => 1,
      'state' => StockUnitStateRecord::DELETED,
      'related_tid' => NULL,
      'related_oid' => NULL,
      'related_uid' => 1,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();

    $possible = $stock_unit->getPossibleStates();
    $expected = [
      StockUnitStateRecordInterface::DELETED,
    ];
    $this->assertEquals($expected, $possible);
  }
}

