<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\commerce_stock_local\LocalStockUpdater;
use Drupal\profile\Entity\Profile;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnit;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;
use Drupal\commerce_stock_units\StockUnitStockUpdater;

/**
 * Test all StockUnitManager methods
 * and Ensure the stock transactions are performed on order events using the 
 * commerce_stock_units service instead of the commerce_local_stock service
 *
 * @group commerce_stock_units
 */
class StockUnitManagerTest extends CommerceStockKernelTestBase {

  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The stock service manager.
   *
   * @var $stockServiceManager
   */
  protected $stockServiceManager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // Change the workflow of the default order type.
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $secondaryStockLocation = StockLocation::create([
      'name' => 'Secondary Location',
      'status' => 1,
      'type' => 'default',
    ]);
    $secondaryStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $this->stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $this->stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $this->stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);
    
    $profile = Profile::create([
      'type' => 'customer',
      'uid' => $user->id(),
    ]);
    $profile->save();

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::create([
      'type' => 'default',
      'state' => 'draft',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
    ]);
    $order->save();

    $this->order = $this->reloadEntity($order);

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')
      ->getStorage('commerce_order_item');

    // Add order item.
    $order_item1 = $order_item_storage->createFromPurchasableEntity($this->variation);
    $order_item1->save();
    $order_item1 = $this->reloadEntity($order_item1);
    $order->addItem($order_item1);
    $order->save();
    $this->order = $this->reloadEntity($order);
    
  }

  /** 
   * Test getting all stock units for a product
   *
   * The default call to this method should just return an array of stock units,
   * in order by orginal state, with the current state.
   * 
   * @group getStockUnits
   * @group basic
   */
  public function testGetStockUnits() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
   
    // first, create some stock units
    $stock_units = [];

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 3; $i++) {
      $data = [
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $all_stock_units = $manager->getStockUnits($this->variation);

    $this->assertEquals(count($stock_units), count($all_stock_units));
  }

  /**
   * Test explicitly geting all stock units for a product.
   *
   * @group getStockUnits
   */
  public function testExplicitGetAllStockUnits() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
   
    // first, create some stock units
    $stock_units = [];

    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => time(),
        'transaction_id' => 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
    }

    for ($i = 0; $i < 3; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
    }

    $all_stock_units = $manager->getStockUnits($this->variation, ['search_type' => 'all']);

    $this->assertEquals(count($stock_units), count($all_stock_units));
  }

  /**
   * Test geting available stock units for a product.
   *
   * @group getStockUnits
   * @group available
   */
  public function testGetAvailableStockUnits() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
   
    // first, create some stock units
    $stock_units = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 3; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $available_stock_units = $manager->getStockUnits($this->variation, [], ['state' => StockUnitStateRecordInterface::AVAILABLE]);

    $this->assertEquals(7, count($available_stock_units));
  }

  /**
   * Test geting available stock units at past context for a product.
   *
   * @group getStockUnits
   * @group available
   */
  public function testGetAvailableStockUnitsPastContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
   
    // first, create some stock units
    $stock_units = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    $id = 1;
    foreach ($time as $timestamp) {
      for ($i = 0; $i < 3; $i++) {
        $data = [
          'product_variation' => $this->variation,
          'location' => $this->locations[1],
          'location_zone' => '',
          'unit_cost' => 10.00,
          'currency_code' => 'CAD',
          'state' => StockUnitStateRecordInterface::AVAILABLE,
          'effective_time' => $timestamp,
          'transaction_id' => $id,
          'related_tid' => NULL,
          'related_oid' => NULL,
          'related_uid' => NULL,
        ];
        $stock_units[] = $manager->createStockUnit($data);
        $id++;
      }
    }

    for ($i = 0; $i < 1; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $available_stock_units = $manager->getStockUnits($this->variation, ['search_time' => strtotime('Dec 16, 2018')], ['state' => StockUnitStateRecordInterface::AVAILABLE]);

    $this->assertEquals(6, count($available_stock_units));
  }

  /**
   * Test getStockUnits() search by location zone.
   *
   * @group getStockUnits
   * @group location_zone
   */
  public function testGetStockUnitsByLocationZoneCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => 'shelf2',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
      $id++;
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all', time()], $value = ['location_zone' => 'shelf2']);
    $this->assertEquals(5, count($found));
  }

  /**
   * Test getStockUnits() search by location zone with past context.
   *
   * @group getStockUnits
   * @group location_zone
   */
  public function testGetStockUnitsByLocationZonePastContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => 'shelf2',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
      $id++;
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Jan 1, 2019')], $value = ['location_zone' => 'shelf2']);
    $this->assertEquals(0, count($found));

    $found = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Jan 1, 2019')], $value = ['location_zone' => '']);
    $this->assertEquals(10, count($found));
  }

  /**
   * Test getStockUnits() search by location.
   *
   * @group getStockUnits
   * @group location
   */
  public function testGetStockUnitsByLocationCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $i + 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[2],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $i + 11,
        'related_tie' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all'], $value = ['location' => $this->locations[2]]);
    $this->assertEquals(5, count($found));
  }

  /**
   * Test getStockUnits() search by location with a past context.
   *
   * @group getStockUnits
   * @group location
   */
  public function testGetStockUnitsByLocationPastContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $i + 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[2],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $i + 11,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all', 'search_time' => strtotime('Jan 1, 2019')], $value = ['location' => $this->locations[2]]);
    $this->assertEquals(0, count($found));

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all', 'search_time' => strtotime('Jan 1, 2019')], $value = ['location' => $this->locations[1]]);
    $this->assertEquals(10, count($found));
  }

  /**
   * Test getStockUnits() search by unit cost.
   *
   * @group getStockUnits
   * @group unit_cost
   */
  public function testGetStockUnitsByUnitCostCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $i + 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[2],
        'location_zone' => '',
        'unit_cost' => 12.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $i + 11,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all'], $value = ['unit_cost' => 10.00]);
    $this->assertEquals(5, count($found));
  }

  /**
   * Test getStockUnits() search by currency code.
   *
   * @group getStockUnits
   * @group currency_code
   */
  public function testGetStockUnitsByUnitCurrencyCodeCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $i + 1,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[2],
        'location_zone' => '',
        'unit_cost' => 12.00,
        'currency_code' => 'USD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time2,
        'transaction_id' => $i + 11,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_type' => 'all'], $value = ['currency_code' => 'USD']);
    $this->assertEquals(5, count($found));
  }

  /**
   * Test getStockUnits search by effective_time
   *
   * @group getStockUnits
   * @group effective_time
   */
  public function testGetStockUnitsByTransactionTimeCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];
    
    $id = 1;
    foreach ($time as $timestamp) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $timestamp,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];

      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 2; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $search_stock_units = $manager->getStockUnits($this->variation, $context = [], $value = ['effective_time' => strtotime('Aug 10, 2019')]);

    $this->assertEquals(1, count($search_stock_units));
    $this->assertEquals($stock_units[3]->id(), $search_stock_units[0]->id());
  }

  /**
   * Test getStockUnits search by effective_time
   * with failing search for old state
   *
   * @group getStockUnits
   * @group effective_time
   */
  public function testGetStockUnitsByTransactionTimeCurrentContextOldState() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    $id = 1;
    foreach ($time as $timestamp) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $timestamp,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];

      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 2; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $search_stock_units = $manager->getStockUnits($this->variation, $context = [], $value = ['effective_time' => strtotime('Dec 1, 2018')]);

    $this->assertEquals(1, count($search_stock_units));
  }

  /**
   * Test getStockUnits search by effective_time
   * with failing search within past context
   *
   * @group getStockUnits
   * @group effective_time
   */
  public function testGetStockUnitsByTransactionTimePastContextOldStateFail() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    $id = 1;
    foreach ($time as $timestamp) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $timestamp,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];

      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 2; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $search_stock_units = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Feb 14, 2019')], $value = ['effective_time' => strtotime('Feb 15, 2018')]);

    $this->assertEquals(0, count($search_stock_units));
  }

  /**
   * Test getStockUnits search by effective_time
   * with past context.
   *
   * @group getStockUnits
   * @group effective_time
   */
  public function testGetStockUnitsByTransactionTimePastContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $id = 1;
    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    foreach ($time as $timestamp) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $timestamp,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];

      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 2; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $search_stock_units = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Feb 16, 2019')], $value = ['effective_time' => strtotime('Feb 15, 2019')]);

    $this->assertEquals(1, count($search_stock_units));
    $this->assertEquals($stock_units[2]->id(), $search_stock_units[0]->id());
  }

  /**
   * Test getStockUnits search by effective_time
   * with past context - failing search.
   *
   * @group getStockUnits
   * @group effective_time
   */
  public function testGetStockUnitsByTransactionTimePastContextFail() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = [
      strtotime('Dec 1, 2018'),
      strtotime('Dec 15, 2018'),
      strtotime('Feb 15, 2019'),
      strtotime('Aug 10, 2019'),
    ];

    $id = 1;
    foreach ($time as $timestamp) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $timestamp,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];

      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    for ($i = 0; $i < 2; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $manager->updateStockUnit($stock_units[$i], $data);  
      $id++;
    }

    $search_stock_units = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Feb 16, 2019')], $value = ['effective_time' => strtotime('Dec 2, 2018')]);

    $this->assertEquals(0, count($search_stock_units));
  }

  /**
   * Test getStockUnits() search by current state.
   *
   * @group getStockUnits
   * @group state
   */
  public function testGetStockUnitsByStateCurrentContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    $id = 1;
    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 4; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => $time2,
        'transaction_id' => $id, 
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
      $id++;
    }

    $found = $manager->getStockUnits($this->variation, [], ['state' => StockUnitStateRecordInterface::AVAILABLE]);
    $this->assertEquals(1, count($found));
  }

  /**
   * Test getStockUnits() search by past state.
   *
   * @group getStockUnits
   * @group state
   */
  public function testGetStockUnitsByStatePastContext() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => $time2,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
      $id++;
    }

    $found = $manager->getStockUnits($this->variation, $context = ['search_time' => strtotime('Feb 26, 2019')], $value = ['state' => StockUnitStateRecordInterface::AVAILABLE]);
    $this->assertEquals(10, count($found));
  }

  /**
   * Test getStockUnits() search by multiple states.
   *
   * @group getStockUnits
   * @group state
   */
  public function testGetStockUnitsByMultipleStates() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $stock_units = [];
    $modified = [];

    $time = strtotime('Dec 10, 2018');

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => $time,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $time2 = strtotime('Feb 28, 2019');

    for ($i = 0; $i < 5; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::SOLD,
        'effective_time' => $time2,
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $modified[] = $manager->updateStockUnit($stock_units[$i], $data);
      $id++;
    }

    $found = $manager->getStockUnits($this->variation, $context = [], $value = ['state' => [StockUnitStateRecordInterface::AVAILABLE, StockUnitStateRecordInterface::SOLD]]);
    $this->assertEquals(10, count($found));
  }

  /**
   * Test getStockUnits() search by source document (an order in this case).
   *
   * @group getStockUnits
   * @group source_document
   */
  public function testGetStockUnitsBySourceDocument() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = StockUnitStockUpdater::create(\Drupal::getContainer());
    
    // first, create some stock units
    $stock_units = [];

    $id = 1;
    for ($i = 0; $i < 10; $i++) {
      $data = [
        'product_variation' => $this->variation,
        'location' => $this->locations[1],
        'location_zone' => '',
        'unit_cost' => 10.00,
        'currency_code' => 'CAD',
        'state' => StockUnitStateRecordInterface::AVAILABLE,
        'effective_time' => time(),
        'transaction_id' => $id,
        'related_tid' => NULL,
        'related_oid' => NULL,
        'related_uid' => NULL,
      ];
      $stock_units[] = $manager->createStockUnit($data);
      $id++;
    }

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    // should sell stock unit 1
    $metadata = [
      'related_oid' => $order->id(),
    ];

    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($this->variation, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $this->assertTrue($transaction_id);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->related_oid);
    $this->assertEquals($order->id(), $transaction->related_oid);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_units_state_record} WHERE source_document_id = :id', array(':id' => $order->id()))->fetchAll();
    $this->assertGreaterThan(0, count($result));
    $found = $manager->getStockUnits($this->variation, $context = [], $value = ['source_document' => $order]);
    $this->assertEquals(1, count($found));
  }


  /**
   * Test the createStockUnit() method.
   *
   * @group createStockUnit
   */
  public function testCreateStockUnit() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => $this->order->id(),
      'source_document_type' => $this->order->getEntityTypeId(),
      'related_uid' => NULL,
    ];
    $stock_unit = $manager->createStockUnit($data);

    $this->assertEquals(1, $stock_unit->id());

    $state_record = $stock_unit->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(1, $state_record->getState());

  }

  /**
   * Test the createStockUnit() method with a field added to the state record.
   *
   * @group createStockUnit
   * @group fields
   */
  public function testCreateStockUnitWithField() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    FieldStorageConfig::create([
      'entity_type' => 'commerce_stock_units_state_rcrd',
      'field_name' => 'wholesale_cost',
      'type' => 'commerce_price',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'commerce_stock_units_state_rcrd',
      'field_name' => 'wholesale_cost',
      'bundle' => 'commerce_stock_units_state_rcrd',
    ])->save();

    $wholesale_cost = new Price('10.00', 'CAD');

    $timestamp = time();
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => $this->order->id(),
      'source_document_type' => $this->order->getEntityTypeId(),
      'related_uid' => NULL,
      'wholesale_cost' => $wholesale_cost,
    ];
    $stock_unit = $manager->createStockUnit($data);

    $this->assertEquals(1, $stock_unit->id());

    $state_record = $stock_unit->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(1, $state_record->getState());
    $this->assertEquals($wholesale_cost, $state_record->get('wholesale_cost')->value);

  }

  /**
   * Test that processTransaction() with NEW_STOCK transaction type creates a new stock unit and state.
   *
   * @group processTransaction
   * @group new_stock
   */
  public function testProcessTransactionCreatesNewStockUnit() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals(1, $stock_units[0]->id());

    $state_record = $stock_units[0]->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(1, $state_record->getState());
  }

  /**
   * Test that processTransaction() with NEW_STOCK transaction type
   * and a related source document id (for future use)
   * creates a new stock unit and saves the relevant source document
   */
  

  /**
   * Test that processTransaction() with STOCK_SALE transaction type updates the proper stock unit.
   *
   * @group processTransaction
   * @group stock_sale
   */
  public function testProcessTransactionStockSale() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    // should sell stock unit 1
    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->related_oid);
    $stock_units = $manager->processTransaction($transaction);
    
    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals(1, $stock_units[0]->id());

    $state_record = $stock_units[0]->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(3, $state_record->getState());
    $this->assertEquals($order->id(), $state_record->getSourceDocument()->id());
  }

  /**
   * Test the processTransaction() STOCK_SALE process.
   *
   * @group processTransaction
   * @group process
   */
  public function testProcessTransactionStockSaleProcess() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // should sell stock unit 1

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];

    $stock_units = [];
    $related_order = NULL;
    if ($transaction->related_oid) {
      $related_order = \Drupal\commerce_order\Entity\Order::load($transaction->related_oid);
    }
    $this->assertGreaterThan(0, $related_order->id());
    $related_user = NULL;
    if ($transaction->related_uid) {
      $related_user = \Drupal::entityTypeManager()->getStorage('user')->load($transaction->related_uid);
    }
    $transaction_data = [
      'transaction_id' => $transaction->id,
      'product_variation' => \Drupal::entityTypeManager()->getStorage($transaction->entity_type)->load($transaction->entity_id),
      'location' => \Drupal::entityTypeManager()->getStorage('commerce_stock_location')->load($transaction->location_id),
      'location_zone' => $transaction->location_zone,
      'unit_cost' => $transaction->unit_cost,
      'currency_code' => $transaction->currency_code,
      'effective_time' => $transaction->transaction_time ? $transaction->transaction_time : time(),
      'related_transaction' => $transaction->related_tid,
      'source_document_id' => $related_order->id(),
      'source_document_type' => $related_order->getEntityTypeId(),
      'related_user' => $related_user,
    ];

    if ($transaction->transaction_type_id == StockTransactionsInterface::STOCK_SALE) {
      $available = $manager::getStockUnits($transaction_data['product_variation'], ['search_time' => $transaction_data['effective_time']], ['state' => StockUnitStateRecordInterface::AVAILABLE]);
      

      $sorted = [];
      foreach ($available as $stock_unit) {
        $origin = $stock_unit->getOriginalState();
        $sorted[$origin->getEffectiveTime() . $stock_unit->id()] = $stock_unit;
      }

      ksort($sorted);
      $to_process = array_slice($sorted, 0, $transaction->qty);

      $transaction_data['state'] = StockUnitStateRecordInterface::SOLD;
      foreach ($to_process as $stock_unit) {
        $stock_unit = $manager::updateStockUnit($stock_unit, $transaction_data);
        $stock_units[] = $stock_unit;
      }

    }

    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals(1, $stock_units[0]->id());

    $state_record = $stock_units[0]->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(3, $state_record->getState());
    $order = $state_record->getSourceDocument();
    $this->assertNotNull($order);
  }


  /**
   * Test that processTransaction() with STOCK_RETURN transaction type updates the proper stock unit.
   * 
   * @group processTransaction
   * @group stock_return
   */
  public function testProcessTransactionStockReturn() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = StockUnitStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
    $metadata = [];
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // should sell stock unit 1
    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();
    
    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    /*
    $metadata = [
      'related_oid' => $order->id(),
    ];
     */
    $transaction_type_id = StockTransactionsInterface::STOCK_RETURN;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $returned_stock_units = $manager->processTransaction($transaction);
    
    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals($returned_stock_units[0]->id(), $stock_units[0]->id());

    $state_record = $returned_stock_units[0]->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(1, $state_record->getState());
  }


  /**
   * Test that processTransaction() with MOVEMENT_TO() transaction type updates the proper stock unit.
   *
   * MOVEMENT_TO would update the location and zone of a stock unit, but could be used to 
   * update the stock unit state as well (e.g. to SHIPPED), and almost always should be
   * provided with a list of stock units to move.
   *
   * Use the data field in the transaction to store the stock unit ids, then load them when processing
   * the transaction. 
   */
  public function testProcessTransactionMovementToShippedState() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    // should sell stock unit 1
    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->related_oid);
    $stock_units = $manager->processTransaction($transaction);
    $this->assertGreaterThan(0, count($stock_units));

    $metadata = [
      'data' => [
        'stock_units' => $stock_units,
        'state' => StockUnitStateRecordInterface::SHIPPED,
      ],
    ];

    // every movement_to transaction should have a matching movement_from transaction, so create that first
    $transaction_type_id = StockTransactionsInterface::MOVEMENT_FROM;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $manager->processTransaction($transaction);

    $transaction_type_id = StockTransactionsInterface::MOVEMENT_TO;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $this->assertGreaterThan(0, $transaction_id);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->id);
    $this->assertTrue(isset($transaction->data));
    $data = unserialize($transaction->data);
    $this->assertTrue(isset($data['stock_units']));
    $this->assertGreaterThan(0, count($data['stock_units']));
    $stock_units = $manager->processTransaction($transaction);

    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals(1, $stock_units[0]->id());

    $state_record = $stock_units[0]->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(StockUnitStateRecordInterface::SHIPPED, $state_record->getState());
  }


  /**
   * Test that processTransaction() with MOVEMENT_TO transaction type and an effective date updates the proper stock unit.
   *
   * MOVEMENT_TO would update the location and zone of a stock unit, but could be used to 
   * update the stock unit state as well (e.g. to SHIPPED), and almost always should be
   * provided with a list of stock units to move.
   *
   * Use the data field in the transaction to store the stock unit ids, target state, and effective date,, 
   * then load them when processing the transaction. 
   */
  public function testProcessTransactionMovementToEffectiveDate() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    // should sell stock unit 1
    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->related_oid);
    $stock_units = $manager->processTransaction($transaction);
    $this->assertGreaterThan(0, count($stock_units));

    $date = strtotime('+1 month');

    $metadata = [
      'data' => [
        'stock_units' => $stock_units,
        'state' => StockUnitStateRecordInterface::SHIPPED,
        'effective_time' => $date,
      ],
    ];

    // every movement_to transaction should have a matching movement_from transaction, so create that first
    $transaction_type_id = StockTransactionsInterface::MOVEMENT_FROM;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $manager->processTransaction($transaction);

    $transaction_type_id = StockTransactionsInterface::MOVEMENT_TO;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $this->assertGreaterThan(0, $transaction_id);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->id);
    $this->assertTrue(isset($transaction->data));
    $data = unserialize($transaction->data);
    $this->assertTrue(isset($data['stock_units']));
    $this->assertGreaterThan(0, count($data['stock_units']));
    $stock_units = $manager->processTransaction($transaction);

    $this->assertEquals(1, count($stock_units)); 
    $this->assertEquals(1, $stock_units[0]->id());

    $state_record = $stock_units[0]->getState($date);
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(StockUnitStateRecordInterface::SHIPPED, $state_record->getState());
    $this->assertEquals($date, $state_record->getEffectiveTime());
  }


  /**
   * Test that processTransaction() checks the future state.
   */
  public function testProcessTransactionAgainstFutureState() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
    $updater = LocalStockUpdater::create(\Drupal::getContainer());

    // stock unit 1 
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);

    /*
    // stock unit 2
    $entity = $this->variation;
    $location_id = 1;
    $zone = '';
    $quantity = 1;
    $unit_cost = '10.00';
    $currency_code = 'CAD';
    $transaction_type_id = StockTransactionsInterface::NEW_STOCK;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata = []);
    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $stock_units = $manager->processTransaction($transaction);
     */

    $order_type = \Drupal\commerce_order\Entity\OrderType::create([
      'status' => TRUE,
      'id' => 'custom_order_type',
      'label' => 'My custom order type',
      'workflow' => 'order_default',
      'refresh_mode' => 'always',
      'refresh_frequency' => 30,
    ]);
    $order_type->save();

    // This must be called after saving.
    commerce_order_add_order_items_field($order_type); 
    
    $order_item_type = \Drupal\commerce_order\Entity\OrderItemType::create([
      'id' => 'custom_order_item_type',
      'label' => 'My custom order item type',
      'status' => TRUE,
      'purchasableEntityType' => 'commerce_product_variation',
      'orderType' => 'custom_order_type',
    ]);
    $order_item_type->save();
    
    $order_item = \Drupal\commerce_order\Entity\OrderItem::create([
      'type' => 'custom_order_item_type',
      'purchased_entity' => $this->variation,
      'quantity' => 1,
      'unit_price' => $this->variation->getPrice(),
    ]);
    $order_item->save();
    
    // Create the billing profile.
    $profile = \Drupal\profile\Entity\Profile::create([
      'type' => 'customer',
      'uid' => 1,
    ]);
    $profile->save();

    // Next, we create the order.
    $order = \Drupal\commerce_order\Entity\Order::create([
      'type' => 'custom_order_type',
      'state' => 'draft',
      'mail' => 'user@example.com',
      'uid' => 1,
      'ip_address' => '127.0.0.1',
      'order_number' => '44444',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item],
      'placed' => time(),
    ]);
    $order->save();

    // should sell stock unit 1
    $metadata = [
      'related_oid' => $order->id(),
    ];
    $transaction_type_id = StockTransactionsInterface::STOCK_SALE;
   
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $this->assertGreaterThan(0, $transaction->related_oid);
    $stock_units = $manager->processTransaction($transaction);
    $this->assertGreaterThan(0, count($stock_units));

    print 'Creating a future SHIPPED transaction' . "\n";
    $date = strtotime('+1 month');

    $metadata = [
      'data' => [
        'stock_units' => $stock_units,
        'state' => StockUnitStateRecordInterface::SHIPPED,
        'effective_time' => $date,
      ],
    ];

    // every movement_to transaction should have a matching movement_from transaction, so create that first
    $transaction_type_id = StockTransactionsInterface::MOVEMENT_FROM;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $manager->processTransaction($transaction);

    $transaction_type_id = StockTransactionsInterface::MOVEMENT_TO;
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $this->assertGreaterThan(0, $transaction_id);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $data = unserialize($transaction->data);
    $stock_units = $manager->processTransaction($transaction);

    #$state_record = $stock_units[0]->getState($date);

    // try to create a new transaction with a shipped state.
    $metadata['data']['stock_units'] = $stock_units;
    unset($metadata['data']['effective_time']);
    $transaction_id = $updater->createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $this->assertGreaterThan(0, $transaction_id);

    $result  = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];
    $data = unserialize($transaction->data);

    // This should fail.
    $stock_units = $manager->processTransaction($transaction);
    $this->assertEquals(0, count($stock_units));

  }

  /**
   * Test that getStateType() returns the correct state record state type.
   * For now just tests a basic conversion.
   *
   * @group getStateType
   */
  public function testGetStateType() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    // basic state conversions
    $this->assertEquals(StockUnitStateRecordInterface::AVAILABLE, $manager::getStateType(StockTransactionsInterface::STOCK_IN));
    $this->assertEquals(StockUnitStateRecordInterface::SOLD, $manager::getStateType(StockTransactionsInterface::STOCK_OUT));

    $this->assertEquals(StockUnitStateRecordInterface::AVAILABLE, $manager::getStateType(StockTransactionsInterface::NEW_STOCK));
    $this->assertEquals(StockUnitStateRecordInterface::AVAILABLE, $manager::getStateType(StockTransactionsInterface::STOCK_RETURN));
    $this->assertEquals(StockUnitStateRecordInterface::SOLD, $manager::getStateType(StockTransactionsInterface::STOCK_SALE));

    $this->assertEquals(StockUnitStateRecordInterface::AVAILABLE, $manager::getStateType(StockTransactionsInterface::MOVEMENT_TO));
    $this->assertEquals(StockUnitStateRecordInterface::AVAILABLE, $manager::getStateType(StockTransactionsInterface::MOVEMENT_FROM));
  }

  /**
   * Test that fields added to products get included in the state record..
   * Incomplete
   *
   * @group field
   */
  public function testProductAddedField() {
    $manager = \Drupal::service('commerce_stock_units.stock_unit_manager');

    $timestamp = time();

    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => $this->order->id(),
      'source_document_type' => $this->order->getEntityTypeId(),
      'related_uid' => NULL,
    ];
    $stock_unit = $manager->createStockUnit($data);

    $this->assertEquals(1, $stock_unit->id());

    $state_record = $stock_unit->getState();
    $this->assertGreaterThan(0, $state_record->id());
    $this->assertEquals(1, $state_record->getState());

  }

}

