<?php

namespace Drupal\Tests\commerce_stock_units\Kernel;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_stock\EventSubscriber\OrderEventSubscriber;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_local\Entity\StockLocation;
use Drupal\profile\Entity\Profile;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Tests\commerce_stock\Kernel\CommerceStockKernelTestBase;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;

/**
 * Tests to help figure out the process of the field config.
 * Should be removed at some point.
 *
 * @group commerce_stock_units
 *
 */
class StockUnitsConfigTest extends CommerceStockKernelTestBase {
  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A test product variation
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * A sample order.
   * 
   * @var \Drupal\commerce_stock\Entity\OrderInterface
   */
  protected $order;

  /**
   * The stock checker.
   *
   * @var \Drupal\commerce_stock\StockCheckInterface
   */
  protected $checker;

  /**
   * An array of location ids for $variation.
   *
   * @var int[]
   */
  protected $locations;

  /**
   * A sample user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The Stock Unit Manager.
   */
  protected $stock_unit_manager;

  /**
   * Modules to enable;
   *
   * @var array
   */
  public static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_store',
    'commerce_stock_local',
    'commerce_stock_units',
    'test_stock_receipt',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product_variation_type');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_type');
    $this->installConfig(['commerce_product']);
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('commerce_order');
    $this->installEntitySchema('commerce_stock_location_type');
    $this->installEntitySchema('commerce_stock_location');
    $this->installConfig(['commerce_stock']);
    $this->installConfig(['commerce_stock_local']);
    $this->installSchema('commerce_stock_local', [
      'commerce_stock_transaction_type',
      'commerce_stock_transaction',
      'commerce_stock_location_level',
    ]);
    $this->installEntitySchema('commerce_stock_units_stock_unit');
    $this->installEntitySchema('commerce_stock_units_state_rcrd');
    $this->installConfig(['commerce_stock_units']);

    // install the Test Stock Recept module
    $this->installEntitySchema('stock_receipt');
    $this->installEntitySchema('stock_receipt_item');
    $this->installConfig(['test_stock_receipt']);
    
    // Change the workflow of the default order type.
    $order_type = OrderType::load('default');
    $order_type->setWorkflowId('order_fulfillment_validation');
    $order_type->save();

    $defaultStockLocation = StockLocation::create([
      'name' => 'Test',
      'status' => 1,
      'type' => "default",
    ]);
    $defaultStockLocation->save();

    $user = $this->createUser();
    $user = $this->reloadEntity($user);
    $this->user = $user;

    $config = \Drupal::configFactory()
      ->getEditable('commerce_stock.service_manager');
    $config->set('default_service_id', 'stock_unit_stock');
    $config->save();
    $stockServiceManager = \Drupal::service('commerce_stock.service_manager');

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $this->variation = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => [
        'number' => '11.11',
        'currency_code' => 'USD',
      ],
    ]);
    $this->variation->save();
    $this->variation = $this->reloadEntity($this->variation);

    $this->product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$this->variation,],
    ]);
    $this->product->save();

    $this->checker = $stockServiceManager->getService($this->variation)
      ->getStockChecker();
    $stockServiceConfiguration = $stockServiceManager->getService($this->variation)
      ->getConfiguration();

    $context = new Context($user, $this->store);
    $this->locations = $stockServiceConfiguration->getAvailabilityLocations($context, $this->variation);

    $this->stock_unit_manager = \Drupal::service('commerce_stock_units.stock_unit_manager');
  }

  /**
   * Test getting all purchasable entity types.
   *
   * @group process
   */
  public function testGetAllPurchasableEntityTypes() {
    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    $product_types = [];
    foreach ($entity_type_definitions as $def) {
      $r = new \ReflectionClass($def->getClass());
      if ($r->implementsInterface('\Drupal\commerce\PurchasableEntityInterface')) {
        $product_types[$def->id()] = $def;
      }  
    }

    $expected['commerce_product_variation'] = \Drupal::entityTypeManager()->getDefinition('commerce_product_variation');

    $this->assertEquals($expected, $product_types);
      
  }

  /**
   * Test getting all entities with a field that references a purchasable entity type.
   *
   * @group process
   */
  public function testGetAllEntitiesThatReferencePurchasableEntities() {
    $defs = \Drupal::entityTypeManager()->getDefinitions();
    $ptypes = [];
    $entities = [];

    foreach ($defs as $def) {
      $r = new \ReflectionClass($def->getClass());
      if ($r->implementsInterface('\Drupal\commerce\PurchasableEntityInterface')) {
        $ptypes[$def->id()] = $def;
      }
    }
    
    $fields = \Drupal::service('entity_field.manager')->getFieldMapByFieldType('entity_reference');

    foreach ($fields as $entity => $field_items) {
      foreach ($field_items as $f => $v) {
        foreach ($v['bundles'] as $bundle) {
          foreach (\Drupal::service('entity_field.manager')->getFieldDefinitions($entity, $bundle) as $name => $def) {
            foreach ($ptypes as $i => $j) {
              if ($def->getType() == 'entity_reference' && $def->getSetting('target_type') == $i) {
                $entities[$entity][$bundle][$name] = $def;
              }
            }
          }
        }
      }
    }

    foreach ([['commerce_product', 'default', 'variations'], ['commerce_order_item', 'default', 'purchased_entity'], ['commerce_stock_units_stock_unit', 'commerce_stock_units_stock_unit', 'product_variation_id'], ['stock_receipt_item', 'stock_receipt_item', 'received_entity']] as $values) {
      $expected[$values[0]][$values[1]][$values[2]] = \Drupal::service('entity_field.manager')->getFieldDefinitions($values[0], $values[1])[$values[2]];
    }

    $this->assertEquals($expected, $entities);
  }

  /**
   * Test copying fields from entities to the state record entity.
   *
   * @group process
   */
  public function testCopyFieldsToStateRecord() {
    // get the wholesale_cost field from the stock_receipt_item entity
    $entity_field_manager = \Drupal::service('entity_field.manager');

    // include two fields, one is a base field and the other an added field
    $config = [['commerce_order_item', 'default', 'unit_price'], ['stock_receipt_item', 'stock_receipt_item', 'wholesale_cost']];

    foreach ($config as $item) {
      $fields = $entity_field_manager->getFieldDefinitions($item[0], $item[1]);
      $new_field_name = $item[2];

      // the field definition might be a BaseFieldefinition which doesn't get duplicated..
      $r = new \ReflectionClass($fields[$item[2]]);
      if ($r->getShortName() === 'BaseFieldDefinition') {
        $config = $fields[$item[2]]->getConfig($item[1]);
        $storage = $config->getFieldStorageDefinition();
        $r = new \ReflectionClass($storage);
        $properties = [
          'field_name' => $new_field_name,
          'entity_type' => 'commerce_stock_units_state_rcrd',
          'type' => $storage->getType(),
          'settings' => $storage->getSettings(),
          'module' => 'commerce_stock_units',
          'locked' => FALSE,
          'cardinality' => $storage->getCardinality(),
          'translatable' => $storage->isTranslatable(),
          'schema' => $storage->getSchema(),
          'persist_with_no_fields' => FALSE,
          'custom_storage' => $storage->hasCustomStorage(),
          'original' => NULL,
          'enforceIsNew' => TRUE,
        ];
        $new_field_storage = new FieldStorageConfig($properties);
        $new_field_storage->save();

        $new_properties = [
          'bundle' => 'commerce_stock_units_state_rcrd',
          'description' => $config->getDescription(),
          'required' => FALSE,
          'default_value' => $config->get('default_value'),
          'field_type' => $config->get('field_type'),
        ];
        $properties = array_merge($properties, $new_properties);

        $new_field = new FieldConfig($properties);
        $new_field->save();
      } else {
        $field_storage = $entity_field_manager->getFieldStorageDefinitions($item[0]);
        $new_field_storage = $field_storage[$item[2]]->createDuplicate();

        $new_field_storage->set('field_name', $new_field_name);
        $new_field_storage->set('entity_type', 'commerce_stock_units_state_rcrd');

        $new_field_storage->save();

        $new_field = $fields[$item[2]]->createDuplicate();

        // field names are limited to 50 characters
        $r = new \ReflectionClass($new_field);

        $new_field->set('field_name', $new_field_name);
        $new_field->set('entity_type', 'commerce_stock_units_state_rcrd');
        $new_field->set('bundle', 'commerce_stock_units_state_rcrd');

        $new_field->save();

      }
    }
      
    $timestamp = time();
    $wholesale_cost = new Price('10.00', 'CAD');
    $unit_price = new Price('20.00', 'CAD');
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
      'wholesale_cost' => $wholesale_cost,
      'unit_price' => $unit_price,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('wholesale_cost'));
    $this->assertEqual($wholesale_cost, $state_record->get('wholesale_cost')[0]->toPrice());

    $this->assertTrue($state_record->hasField('unit_price'));
    $this->assertEqual($unit_price, $state_record->get('unit_price')[0]->toPrice());

  }

  /**
   * Test the creation of a SourceEntityFieldConfig entity using 
   * OrderItem and StockReceiptItem entities.
   *
   * @group config
   * @group process
   */
  public function testSetSourceEntityFieldConfig() {
    #$config = [['commerce_order_item', 'default', 'unit_price'], ['stock_receipt_item', 'stock_receipt_item', 'wholesale_cost']];
    #
    $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->create([
      'id' => 'commerce_order_item.default.unit_price',
      'label' => 'Entity: commerce_order_item; Bundle: default; Field: unit_price',
      'source_entity' => 'commerce_order_item',
      'source_bundle' => 'default',
      'source_field' => 'unit_price',
      'state_field' => 'unit_price',
    ]); 

    $config->save();

    // when the config is saved, the field should be created,
    // but test that separately.

    print 'id: ' . $config->id();
    $this->assertEquals('commerce_order_item.default.unit_price', $config->id());
  }

  /**
   * Test that the field is created when the config is saved.
   *
   * @group config
   * @group process
   */
  public function testSourceEntityFieldConfigSaveCreatesField() {
    $config = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->create([
      'id' => 'commerce_order_item.default.unit_price',
      'label' => 'Entity: commerce_order_item; Bundle: default; Field: unit_price',
      'source_entity' => 'commerce_order_item',
      'source_bundle' => 'default',
      'source_field' => 'unit_price',
      'state_field' => 'unit_price',
    ]); 

    $config->save();
    
    $timestamp = time();
    $wholesale_cost = new Price('10.00', 'CAD');
    $unit_price = new Price('20.00', 'CAD');
    $data = [
      'product_variation' => $this->variation,
      'location' => $this->locations[1],
      'location_zone' => '',
      'unit_cost' => 10.00,
      'currency_code' => 'CAD',
      'state' => StockUnitStateRecordInterface::AVAILABLE,
      'effective_time' => $timestamp,
      'transaction_id' => 1,
      'source_transaction' => 1,
      'source_document_id' => NULL,
      'source_document_type' => NULL,
      'related_uid' => NULL,
      'wholesale_cost' => $wholesale_cost,
      'unit_price' => $unit_price,
    ];
    $stock_unit = $this->stock_unit_manager->createStockUnit($data);

    $state_record = $stock_unit->getState();

    $this->assertTrue($state_record->hasField('unit_price'));
    $this->assertEqual($unit_price, $state_record->get('unit_price')[0]->toPrice());
  }

}

