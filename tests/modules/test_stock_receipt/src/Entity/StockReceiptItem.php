<?php

namespace Drupal\test_stock_receipt\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the Test Stock Recipt Item entity type
 *
 * This is a test entity type for testing some of the functionality
 * of the Commerce Stock Units module.
 *
 * @ingroup commerce_stock_units
 *
 * @ContentEntityType(
 *   id = "stock_receipt_item",
 *   label = @Translation("Test Stock Receipt Item"),
 *   base_table = "test_stock_receipt_item",
 *   admin_permission = "administer commerce_stock",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *   },
 * )
 */
class StockReceiptItem extends ContentEntityBase {
  
  #use EntityChangedTrait;

  /**
   * Define the base fields for the Test Stock Receipt Item entity.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['received_entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Received entity'))
      ->setDescription(t('The received entity'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_product_variation');

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The received item title'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 512,
      ]);

    $fields['quantity'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Quantity'))
      ->setDescription(t('The number of received units'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 0)
      ->setDefaultValue(1);

    return $fields;
  }

}

