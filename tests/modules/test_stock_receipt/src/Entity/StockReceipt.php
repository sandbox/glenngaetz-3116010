<?php

namespace Drupal\test_stock_receipt\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the Test Stock Recipt entity type
 *
 * This is a test entity type for testing some of the functionality
 * of the Commerce Stock Units module.
 *
 * @ingroup commerce_stock_units
 *
 * @ContentEntityType(
 *   id = "stock_receipt",
 *   label = @Translation("Test Stock Receipt"),
 *   base_table = "test_stock_receipt",
 *   admin_permission = "administer commerce_stock",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class StockReceipt extends ContentEntityBase {

  /**
   * Define the base fields for the Test Stock Receipt entity.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['receipt_items'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Receipt Items'))
      ->setDescription(t('The receipt items'))
      ->setSetting('target_type', 'stock_receipt_item')
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }

}

