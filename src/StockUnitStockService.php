<?php

namespace Drupal\commerce_stock_units;

use Drupal\commerce_stock\StockCheckInterface;
use Drupal\commerce_stock\StockServiceConfigInterface;
use Drupal\commerce_stock\StockServiceInterface;
use Drupal\commerce_stock\StockUpdateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_stock_local\LocalStockService;

/**
 * The stock service class
 */
class StockUnitStockService extends LocalStockService {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_stock_units.stock_units_stock_checker'),
      $container->get('commerce_stock_units.stock_units_stock_updater'),
      $container->get('commerce_stock.local_stock_service_config')
    );
  }

  /**
   * Get the name of the service.
   */
  public function getName() {
    return 'Stock unit stock';
  }

  /**
   * Get the ID of the service.
   */
  public function getID() {
    return 'stock_unit_stock';
  }

}
