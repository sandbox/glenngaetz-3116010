<?php

namespace Drupal\commerce_stock_units;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock\StockCheckInterface;
use Drupal\commerce_stock_local\LocalStockUpdater;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class StockUnitStockUpdater
 */
class StockUnitStockUpdater extends LocalStockUpdater {

  /**
   * The stock unit manager.
   *
   * @var \Drupal\commerce_stock_units\StockUnitManagerInterface
   */
  protected $manager;
  
  /**
   * Constructs the stock unit aware local stock updater.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\commerce_stock\StockCheckInterface $checker
   *   The local stock checker.
   * @param \Drupal\commerce_stock_units\StockUnitManagerInterface $manager
   *   The stock unit manager.
   */
  public function __construct(Connection $database, StockCheckInterface $checker, StockUnitManagerInterface $manager) {
    parent::__construct($database, $checker);
    $this->manager = $manager;
  }

  /**
   * Creates an instance of the stock unit aware local stock checker.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The DI container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('commerce_stock.local_stock_checker'),
      $container->get('commerce_stock_units.stock_unit_manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * LocalStockUpdater->createTransaction() prepares and inserts a table row as a transaction. 
   * We want to use that unchanged but add on the creation or updating of stock units and state 
   * to reflect the transaction.
   */
  public function createTransaction(PurchasableEntityInterface $entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, array $metadata) {

    // @todo: Validate that transaction is possible first
    //$this->manager->validateTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);

    $transaction_id = parent::createTransaction($entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, $metadata);
    $result = \Drupal::database()->query('SELECT * FROM {commerce_stock_transaction} WHERE id = :id', array(':id' => $transaction_id))->fetchAll();
    $transaction = $result[0];

    $stock_units = $this->manager->processTransaction($transaction);

    return $transaction_id;
  }

}

