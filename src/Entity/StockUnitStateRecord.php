<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\EntityContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\UserInterface;


/**
 * Defines the Stock Unit State Record class.
 *
 * Stock Unit State Records hold the dated state for a particular stock unit. Every change in 
 * stock unit state should generate a new state record (e.g. receipt, sale, hold, etc.)
 *
 * @ingroup commerce_stock_units
 *
 * @ContentEntityType(
 *   id = "commerce_stock_units_state_rcrd",
 *   label = @Translation("Stock Unit State Record"),
 *   base_table = "commerce_stock_units_state_record",
 *   admin_permission = "administer commerce_stock",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *    }
 * )
 */
class StockUnitStateRecord extends ContentEntityBase implements StockUnitStateRecordInterface {

  /**
   * {@inheritdoc}
   */
  public function getLocation() {
    return $this->get('location')->referencedEntities()[0];
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationZone() {
    return $this->get('location_zone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitCost() {
    return $this->get('unit_cost')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrencyCode() {
    return $this->get('currency_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEffectiveTime() {
    return $this->get('effective_time')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceDocument() {
    if ($source = \Drupal::EntityTypeManager()->getStorage($this->getSourceDocumentType())->load($this->getSourceDocumentId())) {
      return $source;
    }
  }

  /*
   * Get the source document id.
   * For testing purposes.
   */
  public function getSourceDocumentId() {
    return $this->source_document_id->value;
  }

  /*
   * Get the source document type.
   * For testing purposes.
   */
  public function getSourceDocumentType() {
    return $this->source_document_type->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedUser() {
    return $this->get('related_user')->referencedEntities()[0];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceTransaction() {
    return $this->get('source_transaction')->value;
  }

  /**
   * Checks the given state to ensure it is a valid state.
   */
  private function checkState(int $state) {
    if (!in_array($state, $this->getAllStates())) {
      throw new StockUnitStateRecordStateException($state);
    }
  }

  /**
   * Retrieves all of the state constants.
   */
  public function getAllStates() {
    $reflectionClass = new \ReflectionClass($this);
    return $reflectionClass->getConstants();
  }

  /**
   * {@inheritdoc}
   */ 
  public function getNextStates(int $current_state, array $future_states = []) {
    // if anything has happened in the future for this state,
    // then no changes in state should be allowed.
    $next_states = [];
    if (!empty($future_states)) {
      if ($current_state == self::HOLD && $future_states[array_key_first($future_states)] == self::AVAILABLE) {
      #  return array_merge([$current_state], self::NEXT_STATES[$current_state]);
      }
      return [$current_state];
    }
    else {
      return array_merge([$current_state], self::NEXT_STATES[$current_state]);
    }
  }

  /**
   * {@inheritdoc}
   *
   * The commerce_stock transaction uses the "unit_cost" field to store the price of the item
   * sold or received - so it's always essentially the "sale price" of the item at the
   * time of the transaction.
   */
  public static function baseFieldDefinitions( EntityTypeInterface $entity_type ) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Stock Unit State Record'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Stock Unit State Record'))
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('State'))
      ->setDescription(t('The state of the stock unit'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['effective_time'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Effective time'))
      ->setDescription(t('The effective time of the state record'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['location'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Location'))
      ->setDescription(t('The location of the stock unit'))
      ->setSetting('target_type', 'commerce_stock_location')
      ->setReadOnly(TRUE);

    $fields['location_zone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Locaton zone'))
      ->setDescription(t('The location zone of the stock unit'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['unit_cost'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Unit cost'))
      ->setDescription(t('The sale price of the stock unit'))
      ->setReadOnly(TRUE);

    $fields['currency_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Currency code'))
      ->setDescription(t('The currency of the unit cost'))
      ->setReadOnly(TRUE);

    // source_document_id and source_document_type form the generic reference to a source document
    $fields['source_document_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source document ID'))
      ->setDescription(t('The ID of the source document that triggered the inventory transaction that created this state.'))
      ->setReadOnly(TRUE);

    $fields['source_document_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source document type'))
      ->setDescription(t('The entity type of the source document'))
      ->setReadOnly(TRUE)
      ->setSetting('max_length', 256);

    $fields['source_transaction'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source transaction'))
      ->setDescription(t('The commerce stock transaction that created this state.'))
      ->setReadOnly(TRUE);

    return $fields;
  }

}

