<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for the Stock Unit entity.
 */
interface StockUnitInterface extends ContentEntityInterface {

  /**
   * Get the related state record at a given time, or the present if no timestamp given.
   *
   * @param int $timestamp
   *   The unix timestamp or the search time
   *
   * @return StockUnitStateRecord $state
   *   A fully loaded StockUnitStateRecord entity
   */
  public function getState(int $timestamp = NULL);

  /**
   * Retrieve the full state history, up to the current time or the given timestamp.
   *
   * @param int $timestamp
   *   The unix timestamp or the search time
   *
   * @return array $states
   *   An array of fully loaded StockUnitStateRecord entities, 
   *   in order by effective date.
   */
  public function getStateHistory(int $timestamp = NULL);

  /**
   * Get the original state for this stock unit.
   *
   * @return StockUnitStateRecord $state
   *   A fully loaded StockUnitStateRecord entity
   *   representing the original state of the stock unit.
   */
  public function getOriginalState();

  /**
   * Get the available states for this stock unit at the given time.
   * If the stock unit has been marked as sold in the future,
   * it is not available at the present, but could be moved, etc.
   *
   * @param int $timestamp
   *   The unix timestamp for the search
   *
   * @return array $states
   *   An array of possible state values for this stock unit
   */
  public function getPossibleStates(int $timestamp = NULL);
}

