<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
#use Drupal\user\EntityOwnerInterface;
#use Drupal\Core\Entity\EntityChangedInterface;


/**
 * Defines the interface for Stock Unit State Records
 *
 * @ingroup commerce_stock_units
 */
interface StockUnitStateRecordInterface extends ContentEntityInterface {

  // The possible stock unit states.
  const AVAILABLE = 1;
  const HOLD = 2;
  const SOLD = 3;
  const SHIPPED = 4;
  const DEFECTIVE = 5;
  const RETURNED = 6;
  const DESTROYED = 7;
  const LOST = 8;
  const DELETED = 9; 

  const NEXT_STATES = [
    self::AVAILABLE => [
      self::HOLD,
      self::SOLD,
      self::DEFECTIVE,
      self::RETURNED,
      self::DESTROYED,
      self::LOST,
      self::DELETED,
    ],
    self::HOLD => [
      self::AVAILABLE,
    ],
    self::SOLD => [
      self::AVAILABLE,
      self::SHIPPED,
    ],
    self::SHIPPED => [
      self::AVAILABLE,
    ],
    self::DEFECTIVE => [
      self::RETURNED,
      self::DESTROYED,
      self::DELETED,
    ],
    self::RETURNED => [
      self::DELETED,
    ],
    self::DESTROYED => [
      self::DELETED,
    ],
    self::LOST => [
      self::DELETED,
    ],
    self::DELETED => [],
  ];

  /**
   * Get the location.
   *
   * @return CommerceStockLocation $location
   *   A Commerce Stock Location entity.
   */
  public function getLocation();

  /**
   * Get the location zone.
   *
   * @return string $location_zone
   *   The string representing a location zone.
   */
  public function getLocationZone();

  /**
   * Get the unit cost (which is the sale price of the item).
   * @todo rename this field to sale price
   *
   * @return decimal $unit_cost
   */
  public function getUnitCost();

  /**
   * Get the currency code for the unit cost.
   *
   * @return string $currency_code
   *   The string representing the currency code (e.g. "CAD" or "USD").
   */
  public function getCurrencyCode();

  /**
   * Get the state record's effective time.
   *
   * @return int $effective_time
   *   A unix timestamp representing the effective time for this state.
   */
  public function getEffectiveTime();

  /**
   * Get the state.
   *
   * @return int $state
   *   The integer constant representing the state of the stock unit.
   *
   *   Possible values:
   *   AVAILABLE = 1;
   *   HOLD = 2;
   *   SOLD = 3;
   *   SHIPPED = 4;
   *   DEFECTIVE = 5;
   *   RETURNED = 6;
   *   DESTROYED = 7;
   *   LOST = 8;
   *   DELETED = 9; 
   */
  public function getState();

  /**
   * Get the source document.
   *
   * @return Entity $source_document
   *   A loadeed entity. Uses the values in source_document_id 
   *   and source_document_type to load the entity.
   */
  public function getSourceDocument();

  /**
   * Get the source transaction.
   *
   * @return int $source_transaction
   *   The ID of the source stock transaction that triggered 
   *   the creation of this state record.
   */
  public function getSourceTransaction();
  
  /**
   * Get the possible next states given the current state 
   * and an array of future states.
   *
   * @param int $current_state
   *   The current state
   *
   * @param array $future_states
   *   An array of future states
   *
   * @return array $possible_states
   *   An array of next states
   */
  public function getNextStates(int $current_state, array $future_states = []); 
}

