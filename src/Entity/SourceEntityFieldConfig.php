<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
#use Drupal\commerce_stock_units\SourceEntityFieldConfigInterface;

/**
 * Defines a SourceEntityFieldConfig configuration entity.
 *
 * @ConfigEntityType(
 *   id = "source_entity_field_config",
 *   label = @Translation("Source entity field configuration"),
 *   controllers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigEntityStorage"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "source_entity_type",
 *     "source_bundle",
 *     "source_field",
 *     "state_field",
 *   },
 *   admin_permission = "administer commerce stock",
 * )
 */
class SourceEntityFieldConfig extends ConfigEntityBase implements SourceEntityFieldConfigInterface {
  
  /**
   * The machine-readable ID for this configuration entity.
   * ID should be $sourceEntityType.$sourceBundle.$sourceField
   */
  public $id;

  /**
   * The human-readable label for this configuration entity.
   */
  public $label;

  /**
   * The source entity type.
   */
  protected $source_entity_type;

  /**
   * The source bundle type.
   */
  protected $source_bundle;

  /**
   * The source field.
   */
  protected $source_field;

  /**
   * The destination state field.
   */
  protected $state_field;

  /**
   * {@inheritdoc}
   */
  public function getSourceEntityType() {
    return $this->source_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBundle() {
    return $this->source_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceField() {
    return $this->source_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getStateField() {
    return $this->state_field;
  }

}

