<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining the SourceEntityFieldConfig entity.
 * This config entity stores source entity fields that should be
 * added to Stock Unit State Records.
 */

interface SourceEntityFieldConfigInterface extends ConfigEntityInterface {
  
  /**
   * Get the source entity type.
   */
  public function getSourceEntityType();

  /**
   * Get the source bundle type.
   */
  public function getSourceBundle();

  /**
   * Get the source field.
   */
  public function getSourceField();

  /**
   * Get the destination field.
   */
  public function getStateField();

}
