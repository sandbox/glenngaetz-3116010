<?php

namespace Drupal\commerce_stock_units\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Stock Unit entity type
 *
 * Stock Units are base physical stock items. Each actual separate individual item in
 * inventory should be represented by a separate Stock Unit.
 *
 * @ingroup commerce_stock_units
 *
 * @ContentEntityType(
 *   id = "commerce_stock_units_stock_unit",
 *   label = @Translation("Stock Unit"),
 *   base_table = "commerce_stock_units_stock_unit",
 *   admin_permission = "administer commerce_stock",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "product_variation_id" = "product_variation_id",
 *   }
 * )
 */
class StockUnit extends ContentEntityBase implements StockUnitInterface {

  /**
   * {@inheritdoc}
   */
  public function getState(int $timestamp = NULL) {
    $timestamp = $timestamp ? $timestamp : time();
    $states = [];

    // get an array of all states, keyed by timestamp
    $all_states = $this->get('state')->referencedEntities();
    foreach ($all_states as $state) {
      $states[$state->getEffectiveTime()] = $state;
    }

    // sort the array by the timestamp, in reverse order (most recent first)
    ksort($states);
    $states = array_reverse($states, $preserve_keys = TRUE);

    foreach ($states as $state_timestamp => $state) {
      if ($state_timestamp <= $timestamp) {
        return $state;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStateHistory(int $timestamp = NULL) {
    $timestamp = $timestamp ? $timestamp : time();
    $states = [];

    $all_states = $this->get('state')->referencedEntities();
    foreach ($all_states as $state) {
      if ($state->getEffectiveTime() <= $timestamp) {
        $states[$state->getEffectiveTime()] = $state;
      }
    }

    ksort($states);

    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalState() {
    $states = [];
    $all_states = $this->get('state')->referencedEntities();
    foreach ($all_states as $state) {
      $states[$state->getEffectiveTime()] = $state;
    }

    ksort($states);

    return array_shift($states);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleStates(int $timestamp = NULL) {
    $timestamp = $timestamp ? $timestamp : time();
    $future_states = [];
    $current_state = $this->getState($timestamp);
   
    $all_states = $this->get('state')->referencedEntities();
    foreach ($all_states as $state) {
      if ($state->getEffectiveTime() > $timestamp) {
        $future_states[$state->getEffectiveTime()] = $state->getState();
      }
    }

    ksort($future_states);

    return $current_state->getNextStates($current_state->getState(), $future_states);
  }

  /**
   * Define the base fields for the Stock Unit entity.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Stock Unit entity'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Stock Unit entity'))
      ->setReadOnly(TRUE);

    $fields['product_variation_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product Variation'))
      ->setDescription(t('The product variation this stock unit belongs to'))
      ->setSetting('target_type', 'commerce_product_variation');

    $fields['state'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('State'))
      ->setDescription(t('The state of the stock unit'))
      ->setSetting('target_type', 'commerce_stock_units_state_rcrd')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }

}

