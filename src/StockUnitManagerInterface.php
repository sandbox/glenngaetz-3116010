<?php
  
namespace Drupal\commerce_stock_units;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock_units\Entity\StockUnitInterface;

/**
 * @file
 * Defines the interface for the Stock Unit Manager.
 */
interface StockUnitManagerInterface {

  /**
   * Process a new stock transaction.
   * Accepts the commerce stock transaction object and converts values to be passed to
   * the stock unit create or update methods.
   *
   * @param object $transaction
   *   The transaction values (transactions are just a row in the database, not a class)
   *
   * @return array $stock_units
   *   The new or updated stock units
   */
  static public function processTransaction(object $transaction);

  /**
   * Given the stock transaction type, get the appropriate stock unit state.
   * This isn't used anywhere and should be re-thought. 
   *
   * @param int $transaction_type_id
   *   The transaction's type
   *
   * @param int $source_document_entity_id
   *   The id of the source document (e.g. order or return or receipt, etc)
   *
   * @param string $source_document_entity_type
   *   The type of the source document (e.g. order or eturn or reeipt, etc)
   *
   * @return int $state
   *   The state type
   */
  static public function getStateType (int $transaction_type_id, int $source_document_entity_id = NULL, str $source_document_entity_type = NULL);

  /**
   * Find matching stock units or all current if no search values are passed.
   * Should return the "active" state of the stock unit in the given context.
   *
   * @param PurchasableEntityInterface $product_variation
   *   A product_variation to search within.
   *
   * @param array $search_context
   *   An associative array of context values:
   *     - search_time: the timestamp on which to search (search on and prior (<=)  to this date)
   *
   * @param array $search_values
   *   An associative array of search values with the keys being search fields.
   *   The search fields are state record properties or fields.
   *   The values can be a scalar value or an array. An array of values will result in an "IN" query.
   *   searchable fields:
   *     - location: finds stock units at this location
   *     - location_zone: finds stock units in this location zone
   *     - unit_cost: finds stock units with this unit cost [not sure if this would ever be used]
   *     - currency_code: finds stock units with this currency code [could be useful if multiple currencies are used]
   *     - timestamp: finds stock units with a state with this effective date
   *     - state: finds stock units with this state (array of states)
   *     - arbitrary searches should be possible too, but not yet sure how that would work (state 
   *       records should be able to have additional fields added by other modules)
   *
   * @return array $stock_units
   *   An array of matched stock units
   */
  static public function getStockUnits(PurchasableEntityInterface $product_variation, array $search_context = [], array $search_values = []);

  /**
   * Create a new stock unit with a state record.
   *
   * @param $source_entity
   *   The entity that generated the transaction (e.g. an order).
   *
   * @param array $transaction_data
   *   The processed transaction data.
   *
   * @return $stock_unit
   *   The created stock unit.
   */
  static public function createStockUnit(array $transaction_data);

  /**
   * Update the stock unit with new state.
   *
   * @param $stock_unit
   *   The stock unit to be updated.
   *
   * @param array $transaction_data
   *   The processed transaction_data.
   *
   * @return $stock_unit
   *   The modified stock unit.
   */
  static public function updateStockUnit(StockUnitInterface $stock_unit, array $transaction_data);
}

