<?php

namespace Drupal\commerce_stock_units;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

interface FieldConfigHelperInterface {

  /**
   * Get the entity fields to use in the config form.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by entity, bundle, and field name.
   */
  public function getEntityFields();

  /**
   * Get the parent entities and fields for each of the entities 
   * returned by getEntityFields().
   *
   * @param string $entity_type_id
   *   The entity type ID. Only entity types that implement 
   *   \Drupal\Core\Entity\FieldableEntityInterface are supported.
   *
   * @parm string $bundle
   *   The bundle.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by entity, bundle, and field name.
   */
  public function getParentEntityFields(string $entity_type_id, string $bundle); 
}
