<?php

namespace Drupal\commerce_stock_units;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock\StockCheckInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_stock_local\LocalStockChecker;

/**
 * The stock checker implementation for the stock units module.
 * Extends the LocalStockChecker to make it stock unit aware.
 */
class StockUnitStockChecker extends LocalStockChecker {
  
  /**
   * Gets the location stock level using stock units
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param \Drupal\commerce_stock\StockLocationInterface[] $locations
   *   The stock locations.
   *
   * @return array
   *   Stock level information indexed by location id with these values:
   *     - 'qty': The quantity.
   *     - 'last_transaction': The id of the last transaction.
   */
  public function getLocationStockLevels(PuchasableEntityInterface $product, array $locations, $time = NULL, StockUnitManager $manager) {
    $time = $time ? $time : time();
    $location_levels = [];
    foreach ($locations as $location) {
      $stock_units = $manager->getStockUnits($product, ['search_time' => $time], ['location' => $location, 'state' => StockUnitStateRecordInterface::AVAILABLE]);
      $location_levels[$location->getID()] = [
        'qty' => count($stock_units),
      ];
    }
    return $location_levels;
  }
    
}

