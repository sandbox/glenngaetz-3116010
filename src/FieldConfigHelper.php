<?php

namespace Drupal\commerce_stock_units;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

class FieldConfigHelper implements FieldConfigHelperInterface {

  /**
   * The entity type manager
   */
  protected $entity_type_manager;

  /**
   * The entity field manager;
   */
  protected $entity_field_manager;

  /**
   * Construct a FieldConfigHelper object.
   *
   * @param \Drupal\Core\EntityEntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager
   * @param \Drupal\Core\EntityFieldManagerInterface $entity_field_manager
   *   The Entity Field Manager
   */ 
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entity_type_manager = $entity_type_manager;
    $this->entity_field_manager = $entity_field_manager;
  }

  /**
   * Creates an instance of the FieldConfigHelper.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The DI container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFields() {
    $defs = $this->entity_type_manager->getDefinitions();
    $ptypes = [];
    $entity_fields = [];

    foreach ($defs as $def) {
      $r = new \ReflectionClass($def->getClass());
      if ($r->implementsInterface('\Drupal\commerce\PurchasableEntityInterface')) {
        $ptypes[$def->id()] = $def;
      }
    }

    $fields = $this->entity_field_manager->getFieldMapByFieldType('entity_reference');

    foreach ($fields as $entity => $field_items) {
      foreach ($field_items as $f => $v) {
        foreach ($v['bundles'] as $bundle) {
          $defs = $this->entity_field_manager->getFieldDefinitions($entity, $bundle);
          foreach ($defs as $name => $def) {
            foreach ($ptypes as $i => $j) {
              if ($def->getType() == 'entity_reference' && $def->getSetting('target_type') == $i) {
                #$entity_fields[$entity][$bundle][$name] = $def;
                $entity_fields[$entity][$bundle] = $defs;
              }
            }
          }
        }
      }
    }
    return $entity_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentEntityFields(string $entity_type_id, string $bundle) {
    $entity_fields = [];
    $fields = \Drupal::service('entity_field.manager')->getFieldMapByFieldType('entity_reference');

    foreach ($fields as $entity => $field_items) {
      print 'Entity: ' . $entity . "\n";
      foreach ($field_items as $f => $v) {
        foreach ($v['bundles'] as $v_bundle) {
          print 'Bundle: ' . $v_bundle . "\n";
          $defs = $this->entity_field_manager->getFieldDefinitions($entity, $v_bundle);
          foreach ($defs as $name => $def) {
            print 'Field: ' . $name . "\n";
            if ($def->getType() == 'entity_reference' && $def->getSetting('target_type') == $entity_type_id) {
              print 'Field is entity_reference to ' . $entity_type_id . "\n";
              $entity_fields[$entity][$v_bundle] = $defs;
            }
          }
        }
      }
    }
    return $entity_fields;
  }
}
