<?php
  
namespace Drupal\commerce_stock_units;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock\StockTransactionsInterface;
use Drupal\commerce_stock_units\Entity\StockUnitInterface;
use Drupal\commerce_stock_units\StockUnitManagerInterface;
use Drupal\commerce_stock_units\Entity\StockUnitStateRecordInterface;

/**
 * @file
 * Defines the interface for the Stock Unit Manager.
 */
class StockUnitManager implements StockUnitManagerInterface {

  /**
   * {@inheritdoc}
   */
  static public function processTransaction(object $transaction) {
    if ($transaction->id < 1) {
      return FALSE;
    }

    // If the type is NEW_STOCK, then we just create a new stock unit.
    // Other transaction types will update stock units.
    
    $stock_units = [];

    // Convert the transaction values into transaction_data values that are appropriate 
    // for stock unit state records.
    
    // @todo: break the conversion of transaction to transaction_data into a separate method
    
    $related_order = NULL;
    if ($transaction->related_oid) {
      $related_order = \Drupal\commerce_order\Entity\Order::load($transaction->related_oid);
    }

    $related_user = NULL;
    if ($transaction->related_uid) {
      $related_user = \Drupal::entityTypeManager()->getStorage('user')->load($transaction->related_uid);
    }

    $transaction_data = [
      'transaction_id' => $transaction->id,
      'product_variation' => \Drupal::entityTypeManager()->getStorage($transaction->entity_type)->load($transaction->entity_id),
      'location' => \Drupal::entityTypeManager()->getStorage('commerce_stock_location')->load($transaction->location_id),
      'location_zone' => $transaction->location_zone,
      'unit_cost' => $transaction->unit_cost,
      'currency_code' => $transaction->currency_code,
      'related_transaction' => $transaction->related_tid,
      'source_document_id' => isset($related_order) ? $related_order->id() : NULL,
      'source_document_type' => isset($related_order) ? $related_order->getEntityTypeId() : NULL,
      'related_user' => $related_user,
    ];

    if (isset($transaction->data)) {
      $data = unserialize($transaction->data);
      if (isset($data['stock_units'])) {
        $transaction_data['stock_units'] = $data['stock_units'];
      }
      if (isset($data['state'])) {
        $transaction_data['state'] = $data['state'];
      }
      if (isset($data['effective_time'])) {
        $transaction_data['effective_time'] = $data['effective_time'];
      }
    }

    if (!isset($transaction_data['effective_time'])) {
      $transaction_data['effective_time'] = isset($transaction->effective_time) ? $transaction->effective_time : ($transaction->transaction_time ? $transaction->transaction_time : time());
    }
    $quantity = abs($transaction->qty);
    $available = [];

    // do stuff based on transaction type 
    
    // if the transaction type is STOCK_IN or NEW_STOCK
    if (in_array($transaction->transaction_type_id, 
      [StockTransactionsInterface::STOCK_IN, 
      StockTransactionsInterface::NEW_STOCK]
    )) {
      for ($i = 0; $i < $quantity; $i++) {
        $stock_units[] = self::createStockUnit($transaction_data);
      }
    }
    else {
      // if the transction type is STOCK_SALE or STOCK_RETURN
      if (in_array($transaction->transaction_type_id, 
        [StockTransactionsInterface::STOCK_SALE, 
        StockTransactionsInterface::STOCK_RETURN]
      )) {
        // get available stock units for this product and process them
        if ($transaction->transaction_type_id == StockTransactionsInterface::STOCK_RETURN) {
          if (!$related_order) {
            // @todo This should throw an exception if there is no matching order
            // because there should always be a matching order for a return.
            return FALSE;
          }
          $available = self::getStockUnits($transaction_data['product_variation'], ['search_time' => $transaction_data['effective_time'], 'flags' => ['is_returnable']], ['state' => [StockUnitStateRecordInterface::SOLD, StockUnitStateRecordInterface::SHIPPED], 'source_document' => $related_order]);
        }
        else {
          $available = self::getStockUnits($transaction_data['product_variation'], ['search_time' => $transaction_data['effective_time'], 'flags' => ['is_sellable']], ['state' => StockUnitStateRecordInterface::AVAILABLE]);
        }

      }
      // If the transaction type is STOCK_OUT (a generic removal)
      elseif (in_array($transaction->transaction_type_id, [StockTransactionsInterface::STOCK_OUT,])) {
        $available = self::getStockUnits($transaction_data['product_variation'], ['search_time' => $transaction_data['effective_time'], 'flags' => ['is_sellable']], ['state' => StockUnitStateRecordInterface::AVAILABLE]);
      } 
      // If the transaction type is MOVEMENT_FROM or MOVEMENT_TO
      elseif (in_array($transaction->transaction_type_id, [StockTransactionsInterface::MOVEMENT_FROM, StockTransactionsInterface::MOVEMENT_TO])) {

        // We don't do anything for MOVEMENT_FROM type, since this has no state change.
        // It's the MOVEMENT_TO that matters.
        // This may get used to implement things like holds or post sale movement into SHIPPED state,
        // so the search for available units may be more complicated.
        if ($transaction->transaction_type_id == StockTransactionsInterface::MOVEMENT_TO) {

          // if a list of stock units is passed in the transaction data
          if (isset($transaction_data['stock_units'])) {
            $available = $transaction_data['stock_units'];
          }
          else {
            $available = self::getStockUnits($transaction_data['product_variation'], ['search_time' => $transaction_data['effective_time'], 'flags' => ['is_sellable']], ['state' => StockUnitStateRecordInterface::AVAILABLE]);
          }
        }
      } 

      $stock_units = self::processStockUnits($available, $transaction_data, $quantity, $transaction->transaction_type_id);
    }

    return $stock_units;
  }

  /**
   * Process the stock units
   *
   * @param StockUnits[] $available
   *   The available stock units
   *
   * @param array $transaction_data
   *   The transaction data being processed
   *
   * @param int $quantity
   *   The number of stock units to process
   *
   * @param int $type
   *   The transaction type being processed
   *
   * @return StockUnit[] $stock_units
   *   An array of the updated stock units
   */
  private static function processStockUnits (array $available, array $transaction_data, int $quantity, int $type) {
    if (count($available) < $quantity) {
      // @todo Throw an exception as available quantity should have been checked
      // when the order was placed. If it's wrong now, something is definitely wrong.
      return FALSE;
    }

    $stock_units = [];

    $sorted = [];

    if (!isset($transaction_data['state'])) { 
      $transaction_data['state'] = self::getStateType($type);
    }

    // sort the stock units from oldest to newest so we sell or return the oldest first.
    foreach ($available as $stock_unit) {
      if (in_array($transaction_data['state'], $stock_unit->getPossibleStates($transaction_data['effective_time']))) {
        $origin = $stock_unit->getOriginalState();
        $sorted[$origin->getEffectiveTime() . $stock_unit->id()] = $stock_unit;
      }
    }
    ksort($sorted);
    $to_process = array_slice($sorted, 0, $quantity);

    foreach ($to_process as $stock_unit) {
      $stock_unit = self::updateStockUnit($stock_unit, $transaction_data);
      $stock_units[] = $stock_unit;
    }
    return $stock_units; 
  }

  /**
   * {@inheritdoc}
   */
  static public function getStateType (int $transaction_type_id, int $source_document_entity_id = NULL, str $source_document_entity_type = NULL) {
    if (!$source_document_entity_type) {
      switch ($transaction_type_id) {
        case 1:
          return StockUnitStateRecordInterface::AVAILABLE;
        case 2:
          return StockUnitStateRecordInterface::SOLD;
        case 4:
          return StockUnitStateRecordInterface::SOLD;
        case 5:
          return StockUnitStateRecordInterface::AVAILABLE;
        case 6:
          return StockUnitStateRecordInterface::AVAILABLE;
        case 7:
          return StockUnitStateRecordInterface::AVAILABLE;
        case 8:
          return StockUnitStateRecordInterface::AVAILABLE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static public function getStockUnits(PurchasableEntityInterface $product_variation, array $search_context = [], array $search_values = []) {

    $search_time = isset($search_context['search_time']) ? $search_context['search_time'] : time();

    $query = \Drupal::entityQuery('commerce_stock_units_stock_unit')
      ->condition('product_variation_id', $product_variation->id());

    foreach ($search_values as $field => $value) {
      if ($field === 'location') {
        $field = 'location.entity.location_id';
        if (is_array($value)) {
          foreach ($value as $k => $v) {
            $value[$k] = $v->id();
          }
        }
        else {
          $value = $value->id();
        }
      }

      if ($field === 'source_document') {
        // the source document should be a loaded entity
        if (is_array($value)) {
          $or_group = $query->orConditionGroup();
          foreach ($value as $source) {
            $group = $or_group->andConditionGroup()
              ->condition('state.entity.' . $field . '_id', $source->id())
              ->condition('state.entity.' . $field . '_type', $source->getEntityTypeId());
            $or_group->condition($group);
          }
          $query->condition($group);
        }
        else {
          $group = $query->andConditionGroup()
            ->condition('state.entity.' . $field . '_id', $value->id())
            ->condition('state.entity.' . $field . '_type', $value->getEntityTypeId());
          $query->condition($group);
        }
      }
      else { 
        if (is_array($value)) {
          $query->condition('state.entity.' . $field, $value, 'IN');
        }
        else {
          if ($value == FALSE) {
            $group = $query->orConditionGroup()
              ->condition('state.entity.' . $field, NULL, 'IS NULL')
              ->condition('state.entity.' . $field, $value);
            $query->condition($group);
          }
          else {
            $query->condition('state.entity.' . $field, $value);
          }
        }
      }
    }

    $query->condition('state.entity.effective_time', $search_time, '<=');

    $query->addTag('debug');

    $ids = $query->execute();

    $stock_units = [];

    if (is_array($ids)) {
      $storage = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit');
      foreach ($ids as $id) {
        $stock_unit = $storage->load($id);

        if (isset($search_values['state'])) {
          if ($current_state = $stock_unit->getState($search_time)) {
            if (is_array($search_values['state'])) {
              if (in_array($current_state->getState(), $search_values['state'])) {
                $stock_units[] = $stock_unit;
              }
            }
            else {
              if ($current_state->getState() == $search_values['state']) {
                $stock_units[] = $stock_unit;
              }
            }
          }
        }
        else {
          $stock_units[] = $stock_unit;
        }
      }
    }
    return $stock_units;
  }

  /**
   * {@inheritdoc}
   */
  static public function createStockUnit(array $transaction_data) {
    $stock_unit = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_stock_unit')->create([
      'product_variation_id' => $transaction_data['product_variation']->id(),
    ]);

    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create($transaction_data);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();
    
    return $stock_unit;
  }

  /**
   * {@inheritdoc}
   */
  static public function updateStockUnit(StockUnitInterface $stock_unit, array $transaction_data) {
    $state_record = \Drupal::entityTypeManager()->getStorage('commerce_stock_units_state_rcrd')->create([
      'transaction_id' => $transaction_data['transaction_id'],
      'location' => $transaction_data['location'],
      'location_zone' => $transaction_data['location_zone'],
      'unit_cost' => $transaction_data['unit_cost'],
      'currency_code' => $transaction_data['currency_code'],
      'effective_time' => $transaction_data['effective_time'],
      'state' => $transaction_data['state'],
      'source_transaction' => isset($transaction_data['source_transaction']) ? $transaction_data['source_transaction'] : NULL,
      'source_document_id' => isset($transaction_data['source_document_id']) ? $transaction_data['source_document_id'] : NULL,
      'source_document_type' => isset($transaction_data['source_document_type']) ? $transaction_data['source_document_type'] : NULL,
      'related_user' => isset($transaction_data['related_user']) ? $transaction_data['related_user'] : NULL,
    ]);

    $state_record->save();

    $stock_unit->state[] = $state_record;
    $stock_unit->save();


    return $stock_unit; 
  }

}

