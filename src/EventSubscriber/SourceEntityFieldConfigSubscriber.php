<?php

namespace Drupal\commerce_stock_units\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\profile\Entity\Profile;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityFieldManager;

/**
 * The SourceEntityFieldConfig event subscriber class.
 * Reacts on CRUD of SourceEntityFieldConfig to 
 * create/edit/delete fields on Stock Unit State Records.
 *
 * @package Drupal\commerce_stock_units\EventSubscriber
 */
class SourceEntityFieldConfigSubscriber implements EventSubscriberInterface {
  
  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods to be executed.
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => 'processConfigSave',
      ConfigEvents::DELETE => 'processConfigDelete',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Config CRUD event.
   */
  public function processConfigSave(ConfigCrudEvent $event) {
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $config = $event->getConfig();
    $s = 'commerce_stock_units_state_rcrd';

    $name = explode('.', $config->getName());
    if ($name[1] == 'source_entity_field_config') {
      $id = implode('.', array_slice($name, 2));
      //load up the full config entity
      $config_data = \Drupal::entityTypeManager()->getStorage('source_entity_field_config')->load($id);

      $fields = $entity_field_manager->getFieldDefinitions($config_data->getSourceEntityType(), $config_data->getSourceBundle());
      $s_field_name = $config_data->getStateField();

      // if the field is not already set on the state record entity
      if (!isset($entity_field_manager->getFieldDefinitions($s, $s)[$s_field_name])) {
   
        // the field definition might be a BaseFieldefinition which doesn't get duplicated..
        $r = new \ReflectionClass($fields[$config_data->getSourceField()]);
        if ($r->getShortName() === 'BaseFieldDefinition') {
          $field_config = $fields[$config_data->getSourceField()]->getConfig($config_data->getSourceBundle());
          $storage = $field_config->getFieldStorageDefinition();
          $properties = [
            'field_name' => $s_field_name,
            'entity_type' => $s,
            'type' => $storage->getType(),
            'settings' => $storage->getSettings(),
            'module' => 'commerce_stock_units',
            'locked' => FALSE,
            'cardinality' => $storage->getCardinality(),
            'translatable' => $storage->isTranslatable(),
            'schema' => $storage->getSchema(),
            'persist_with_no_fields' => FALSE,
            'custom_storage' => $storage->hasCustomStorage(),
            'original' => NULL,
            'enforceIsNew' => TRUE,
          ];
          $new_field_storage = new FieldStorageConfig($properties);
          $new_field_storage->save();

          $new_properties = [
            'bundle' => $s,
            'description' => $field_config->getDescription(),
            'required' => FALSE,
            'default_value' => $field_config->get('default_value'),
            'field_type' => $field_config->get('field_type'),
          ];
          $properties = array_merge($properties, $new_properties);

          $new_field = new FieldConfig($properties);
          $new_field->save();
        } else {
          $field_storage = $entity_field_manager->getFieldStorageDefinitions($config_data->getSourceEntityType());
          $new_field_storage = $field_storage[$config_data->getSourceField()]->createDuplicate();

          $new_field_storage->set('field_name', $s_field_name);
          $new_field_storage->set('entity_type', $s);

          $new_field_storage->save();

          $new_field = $fields[$config_data->getSourceField()]->createDuplicate();

          $new_field->set('field_name', $s_field_name);
          $new_field->set('entity_type', $s);
          $new_field->set('bundle', $s);

          $new_field->save();
        }
      }
    }
  }
  
  /**
   * React to a config object being deleted. Nothing should happen,
   * but it might sometime, so this is a stub.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Config CRUD event.
   */
  public function processConfigDelete(ConfigCrudEvent $event) {
    // do nothing

  }
}
